using System;
using System.Collections.Generic;

public class GeneticAlgorithm<T>
{
	public List<Animal<T>> population { get; private set; }
	public int Generation { get; private set; }
	public float BestFitness { get; private set; }
	public T[] BestGenes { get; private set; }

	public int Elitism;
	public float MutationRate;
	
	private List<Animal<T>> newPopulation;
	private Random random;
	private float fitnessSum;
	private int dnaSize;
	private Func<T> getRandomGene;
	private Func<int, float> fitnessFunction;

	public GeneticAlgorithm(int populationSize, int dnaSize, Random random, Func<T> getRandomGene, Func<int, float> fitnessFunction,
		int elitism, float mutationRate = 0.01f)
	{
		Generation = 1;
		Elitism = elitism;
		MutationRate = mutationRate;
		population = new List<Animal<T>>(populationSize);
		newPopulation = new List<Animal<T>>(populationSize);
		this.random = random;
		this.dnaSize = dnaSize;
		this.getRandomGene = getRandomGene;
		this.fitnessFunction = fitnessFunction;

		BestGenes = new T[dnaSize];

		for (int i = 0; i < populationSize; i++)
		{
			population.Add(new Animal<T>(dnaSize, random, getRandomGene, fitnessFunction, shouldInitGenes: true));
		}
	}

	public void NewGeneration(int numNewDNA = 0, bool crossoverNewDNA = false)
	{
		int finalCount = population.Count + numNewDNA;

		if (finalCount <= 0) {
			return;
		}

		if (population.Count > 0) {
			CalculateFitness();
			population.Sort(CompareDNA);
		}
		newPopulation.Clear();

		for (int i = 0; i < population.Count; i++)
		{
			if (i < Elitism && i < population.Count)
			{
				newPopulation.Add(population[i]);
			}
			else if (i < population.Count || crossoverNewDNA)
			{
				Animal<T> parent1 = ChooseParent();
				Animal<T> parent2 = ChooseParent();

				Animal<T> child = parent1.Crossover(parent2);

				child.Mutate(MutationRate);

				newPopulation.Add(child);
			}
			else
			{
				newPopulation.Add(new Animal<T>(dnaSize, random, getRandomGene, fitnessFunction, shouldInitGenes: true));
			}
		}

		List<Animal<T>> tmpList = population;
		population = newPopulation;
		newPopulation = tmpList;

		Generation++;
	}
	
	private int CompareDNA(Animal<T> a, Animal<T> b)
	{
		if (a.fitness > b.fitness) {
			return -1;
		} else if (a.fitness < b.fitness) {
			return 1;
		} else {
			return 0;
		}
	}

	private void CalculateFitness()
	{
		fitnessSum = 0;
		Animal<T> best = population[0];

		for (int i = 0; i < population.Count; i++)
		{
			fitnessSum += population[i].CalculateFitness(i);

			if (population[i].fitness > best.fitness)
			{
				best = population[i];
			}
		}

		BestFitness = best.fitness;
		best.genes.CopyTo(BestGenes, 0);
	}

	private Animal<T> ChooseParent()
	{
		double randomNumber = random.NextDouble() * fitnessSum;

		for (int i = 0; i < population.Count; i++)
		{
			if (randomNumber < population[i].fitness)
			{
				return population[i];
			}

			randomNumber -= population[i].fitness;
		}

		return null;
	}
}
