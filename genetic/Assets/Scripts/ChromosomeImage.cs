﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChromosomeImage : MonoBehaviour, IPointerEnterHandler
{

    public Chromosome chromosome;
    public GameObject guiScript;


	// Use this for initialization
	void Start () {
        guiScript = GameObject.Find("GUIScripts");
    }


    void OnMouseEnter()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        guiScript.GetComponent<GUICore>().toolTip(this.gameObject);
    }
   }
