﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Biome : MonoBehaviour {

    public EnviroGeneration enviroScript;
    public GameObject enviro;
    public int biomeSize;
    public Biomes type;
    public int seed;
    public GameObject tilePrefab;
    public GUICore guiScript;

    public float temperature;
    public float humidity;

	// Use this for initialization
	void Start () {
	}
	

    public void setStats(int biomeSize, int seed, EnviroGeneration enviroScript, GUICore guiScript)
    {
        this.biomeSize = biomeSize;
        this.seed = seed;
        this.enviroScript = enviroScript;
        this.guiScript = guiScript;
    }

    public void createTiles(Biomes biomeType, int xStart, int yStart)
    {
        type = biomeType;
        int i = 1000 * seed;
        float perlinSize = 5.0f;
        GameObject tile = null;
        //       float noise = Mathf.PerlinNoise(1, 1);
        for (int x = xStart; x < biomeSize +xStart; x++)
        {
            for (int y = yStart; y < biomeSize +yStart; y++)
            {
                float ret = Mathf.PerlinNoise((x + i) / perlinSize, (y + i) / perlinSize);
                tile = Instantiate(tilePrefab, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                tile.name = "Tile";
                MouseOver mouseOver = tile.GetComponent<MouseOver>();
                Terrain tileScript = tile.GetComponent<Terrain>();

                mouseOver.guiScript = guiScript;

                if (biomeType == Biomes.Temperate)
                {
                    temperature = UnityEngine.Random.Range(20, 25);
                    humidity = UnityEngine.Random.Range(40, 70);
                    if (ret < 0.2)
                    {
                        tileScript.setStats(TerrainType.Forest,this);
                    }
                    else if (ret < 0.3)
                    {
                        tileScript.setStats(TerrainType.Plain1,this);
                    }
                    else if (ret < 0.4)
                    {
                        tileScript.setStats(TerrainType.Plain2,this);
                    }
                    else if (ret < 0.47)
                    {
                        tileScript.setStats(TerrainType.Plain3,this);
                    }
                    else if (ret < 0.475)
                    {
                        tileScript.setStats(TerrainType.Mountain,this);
                    }
                    else if (ret < 0.65)
                    {
                        tileScript.setStats(TerrainType.Grassland1, this);
                    }
                    else if (ret < 0.9)
                    {
                        tileScript.setStats(TerrainType.Grassland2, this);
                    }
                    else if (ret >= 0.9)
                    {
                        tileScript.setStats(TerrainType.Sea, this);
                    }
                }

                else if(biomeType == Biomes.Marsh)
                {
                    temperature = UnityEngine.Random.Range(9, 18);
                    humidity = UnityEngine.Random.Range(60, 90);
                    if (ret < 0.1)
                    {
                        tileScript.setStats(TerrainType.Forest, this);
                    }
                    else if (ret < 0.2)
                    {
                        tileScript.setStats(TerrainType.Plain2, this);
                    }
                    else if (ret < 0.4)
                    {
                        tileScript.setStats(TerrainType.Plain1, this);
                    }
                    else if (ret < 0.47)
                    {
                        tileScript.setStats(TerrainType.Grassland1, this);
                    }
                    else if (ret < 0.55)
                    {
                        tileScript.setStats(TerrainType.Grassland2, this);
                    }
                    else if (ret < 0.7)
                    {
                        tileScript.setStats(TerrainType.Grassland2, this);
                    }
                    else if (ret >= 0.7)
                    {
                        tileScript.setStats(TerrainType.Sea, this);
                    }
                }

                else if (biomeType == Biomes.Forest)
                {
                    temperature = UnityEngine.Random.Range(12, 20);
                    humidity = UnityEngine.Random.Range(40, 70);
                    if (ret < 0.35)
                    {
                        tileScript.setStats(TerrainType.Forest, this);
                    }

                    else if (ret < 0.4)
                    {
                        tileScript.setStats(TerrainType.Grassland1, this);
                    }

                    else if (ret <0.5)
                    {
                        tileScript.setStats(TerrainType.Plain2, this);
                    }
                    else if (ret < 0.6)
                    {
                        tileScript.setStats(TerrainType.Plain3, this);
                    }
                    else if (ret >= 0.6)
                    {
                        tileScript.setStats(TerrainType.Forest, this);
                    }
                }

                //"Empty" species grids by default contain a reference to the enviro tile in that grid
                tile.GetComponent<Terrain>().enviro = enviroScript;
                enviroScript.addTile(tile, x, y);

            }

        }


    }
}
