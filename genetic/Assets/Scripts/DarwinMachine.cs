﻿using CielaSpike;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;


public enum HowDied
{
    Starvation,
    Disease,
    Unattractive,
    Impotent,
    Violence
}

public enum StageFailed
{
    Childhood,
    Adulthood,
    Reproduction,
    Success,
    Alive
}


public class DarwinMachine : MonoBehaviour {



    public int numCreatures;
    public int numSpecies;
    public int count = 3;
    public int food = 1000;
    public int speciesCount;
    public int basicSpeciesCount;
    public int numberOfGenes = 11;
    public char[] validCharacters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    public List<Creature> creatures = new List<Creature>();
    public List<GameObject> speciesList = new List<GameObject>();
    public List<Creature> maleFit = new List<Creature>();
    public List<Creature> femaleFit = new List<Creature>();
    public List<GameObject> playerGroups = new List<GameObject>();
    public char[] genes;
    public char[] origGenes;
    public GameObject speciesPrefab;
    public GameObject environment;
    public List<Sprite> speciesSprites;
    public int numSpecieSprites;
    public GUICore GUIscript;
    public bool paused;

    public int tickYear;

    public GeneGeneration geneGenerator;

    public GameObject spools;

    public float tickSpeed;

    public Player player;

    public GameObject camera;
    public GameObject selectedGroup;

    // Use this for initialization
    void Start () {
        //     generateCreatures();
        
    }

    public void loadLevel()
    {
        
        loadSprites();
        tickYear = 0;
        environment.GetComponent<EnviroGeneration>().fillMapBiomes();

        paused = true;
        char[] genes = new char[numberOfGenes];
        char[] origGenes = new char[numberOfGenes];
        spools.GetComponent<CreatureSpool>().initialCreatureGeneration();
        spools.GetComponent<ChromosomeSpool>().initialChromosomeGeneration();
        geneGenerator.createGenes();
        generateSpecies();
        //spools.GetComponent<CreatureSpool>().endTick();
        //spools.GetComponent<ChromosomeSpool>().endTick();
        //      camera.GetComponent<CameraController>().LockView(playerGroups[0]);
    }

    void Update()
    {
        if (Input.GetKeyDown("g"))
       {
            loadLevel();
        }

        if (Input.GetKeyDown("l"))
        {
            if (playerGroups.Count > 0)
            {
                camera.GetComponent<CameraController>().LockView(playerGroups[0]);
            }
        }

        if (Input.GetKeyDown("f") && paused)
        {
            paused = false;
                  StartCoroutine("Tick");
        //    singleTick();
   //         Task task;
   //         this.StartCoroutineAsync(Cancellation(), out task);
        }
        else if (Input.GetKeyDown("f") && !paused)
        {
            paused = true;
            StopCoroutine("Tick");
        }

        if (Input.GetKeyDown("d"))
        {
            GUIscript.selected.GetComponent<Group>().splitDrop();
        }

    }

    public void loadSprites()
    {
        for(int i = 1; i <= numSpecieSprites;i++)
        {
            speciesSprites.Add(Resources.Load<Sprite>("Creature Sprites/animal" + i));
        }
    }


    public char GetRandomGene()
    {

        int i = (int)UnityEngine.Random.Range(0, 25);
        return validCharacters[i];
    }

    public void createGenome()
    {
        for (int i = 0; i < genes.Length; i++)
        {
            origGenes[i] = GetRandomGene();
        }

    }

    public void generateSpecies()
    {
        createGenome();
        for (int i = 0; i < speciesCount; i++)
        {
            GameObject species = Instantiate(speciesPrefab, new Vector3(50 + (i*5), 50 + (i * 5), 0), Quaternion.identity) as GameObject;
  //          Vector3 location = environment.GetComponent<EnviroGeneration>().setLocation();
            species.GetComponent<Species>().numCreatures = numCreatures;
            species.GetComponent<Species>().core = this;
            species.GetComponent<Species>().origGene = origGenes;
            species.GetComponent<Species>().count = count;
            species.GetComponent<Species>().numberOfGenes = numberOfGenes;
            species.GetComponent<Species>().environment = environment;
            //Randomly select sprite from list for species
            int spriteIndex = (int)UnityEngine.Random.Range(0, speciesSprites.Count);
            species.GetComponent<Species>().sprite = speciesSprites[spriteIndex];
            //      speciesSprites.RemoveAt(spriteIndex);

            species.GetComponent<Species>().guiScript = GUIscript;

            speciesList.Add(species);
            species.GetComponent<Species>().startLife(false);
        }
        for (int i = 0; i < basicSpeciesCount; i++)
        {
            GameObject species = Instantiate(speciesPrefab, new Vector3(50 + (i * 5), 50 + (i * 5), 0), Quaternion.identity) as GameObject;
            //          Vector3 location = environment.GetComponent<EnviroGeneration>().setLocation();
            species.GetComponent<Species>().core = this;
            species.GetComponent<Species>().count = count;
            species.GetComponent<Species>().environment = environment;
            //Randomly select sprite from list for species
            int spriteIndex = (int)UnityEngine.Random.Range(0, speciesSprites.Count);
            species.GetComponent<Species>().sprite = speciesSprites[spriteIndex];
            //     speciesSprites.RemoveAt(spriteIndex);
            species.GetComponent<Species>().guiScript = GUIscript;

            speciesList.Add(species);
            species.GetComponent<Species>().startLife(true);
        }
      }

    public void addPlayerGroup(GameObject group)
    {
        playerGroups.Add(group);
    }
    public void removePlayerGroup(GameObject group)
    {
        playerGroups.Remove(group);
    }

    public void pause()
    {
        paused = true;
        StopCoroutine("Tick");
    }

    public void singleTick()
    {
        spools.GetComponent<CreatureSpool>().endTick();
        spools.GetComponent<ChromosomeSpool>().endTick();
        Profiler.BeginSample("tick");
        tickYear++;
        for (int i = 0; i < speciesList.Count; i++)
            {
                speciesList[i].GetComponent<Species>().tick();
            }
        Profiler.EndSample();
        for (int i = 0; i < speciesList.Count; i++)
            {
                speciesList[i].GetComponent<Species>().dead();
            }
        for (int i = 0; i < speciesList.Count; i++)
            {
 //               speciesList[i].GetComponent<Species>().split();
            }

        for (int i = 0; i < speciesList.Count; i++)
        {
    //       speciesList[i].GetComponent<Species>().drops();
        }

        Profiler.BeginSample("enviro");
        environment.GetComponent<EnviroGeneration>().tick();
        Profiler.EndSample();
        Profiler.BeginSample("GUI");
        GUIscript.GetComponent<GUICore>().tick();
        Profiler.EndSample();
        Profiler.BeginSample("move");
        for (int i = 0; i < speciesList.Count; i++)
        {
            speciesList[i].GetComponent<Species>().move();
        }
        Profiler.EndSample();
        Profiler.BeginSample("FOV");
        this.gameObject.GetComponent<FOV>().fovNeighbours(playerGroups, environment.GetComponent<EnviroGeneration>().size);
        Profiler.EndSample();
        GUIscript.GetComponent<GUICore>().updatePopGraph(tickYear);
        GUIscript.GetComponent<GUICore>().groupSummary();

    }



    IEnumerator Tick()
    {
        Task task;
        bool test = true;
        while (test == true)
        {
   //         yield return this.StartCoroutineAsync(ErrorHandling(), out task);
            singleTick();
            yield return new WaitForSeconds(tickSpeed);
        }
    }

}
