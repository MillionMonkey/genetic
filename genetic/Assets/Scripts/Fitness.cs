﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fitness : MonoBehaviour {

    public enum Events
    {
        Cold,
        Disease,
        Heat,
        Drought,
        Flood,
        Fire,
    }

    int creatureCount;
    int food;
    Creature creature;
    int chance;
    public int count = 0;

    public bool fitnessTest(Creature creature, int count, int food)
    {
        creatureCount = count;
        this.creature = creature;
        this.food = food;

        bool alive = true;
        alive = childFitness();
        if(alive == false) { return alive; }
        alive = adultFitness();
        if (alive == false) { return alive; }
        alive = reproductionFitness();
        return alive;

    }

    public bool childFitness()
    {
        //Split into when they failed (Childhood, failed to reproduce (no match/stillbirth etc)
        //    count++;
 //       Debug.Log(creature.generation + " child");
 //       Debug.Log(creature.father.generation + " Father");
        //Childhood
        int chance = 100;
        if((creatureCount > 150000))
     //   if (food > ((creatureCount * 0.5)))
        {
                            chance = chance - 20;
        }
        else
        {
    //        chance = chance - 20;
        }

        //This is just for the GUI, likely a bit too much overhead
        //     creature.fitness = chance;
        if (chance < UnityEngine.Random.Range(0, 100))
        {
            creature.diedType = HowDied.Starvation;
            creature.failedType = StageFailed.Childhood;
            creature.core.failCount(StageFailed.Childhood);
            return false;
        }
        return true;
    }

    public bool adultFitness()
    {
        //Adult
        chance = 90;
        if(creatureCount > 150000)
   //     if (food > (creatureCount * (0.2)))
        {
               chance = chance - 25;
        }
        else
        {
   //         chance = chance - 30;
        }

        //This is just for the GUI, likely a bit too much overhead
        //     creature.fitness = chance;
        if (chance < UnityEngine.Random.Range(0, 100))
        {
            creature.diedType = HowDied.Starvation;
            creature.failedType = StageFailed.Adulthood;
            creature.core.failCount(StageFailed.Adulthood);
            return false;
        }
        return true;
    }

    public bool reproductionFitness()
    {
        //Selection
        chance = 100;

        //This is just for the GUI, likely a bit too much overhead
        //     creature.fitness = chance;
        if (chance < UnityEngine.Random.Range(0, 100))
        {
            creature.diedType = HowDied.Impotent;
            creature.failedType = StageFailed.Reproduction;
            creature.core.failCount(StageFailed.Reproduction);
            return false;
        }
        return true;

    }
}
