﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour {

    public GameObject parentTile;

    public SpriteRenderer renderer;
    public SpriteRenderer parentRenderer;

    public AnimationScript anim;

    public Sprite tileSprite;

	// Use this for initialization
	void Start () {


    }


    //Called via animation
    public void changeSprite()
    {
            renderer = gameObject.GetComponent<SpriteRenderer>();
            renderer.sprite = tileSprite;
    }

    public void changeUnderSprite()
    {
        parentRenderer = parentTile.GetComponent<SpriteRenderer>();
        parentRenderer.sprite = tileSprite;
    }

    public void callEnviro()
    {
        parentTile.GetComponent<Terrain>().callEnviro();
    }

    public void playAnimation()
    {
        Animation anim = GetComponent<Animation>();
        anim.Play("TileSize");
    }

    public void playImpactAnimation()
    {

        Animation anim = GetComponent<Animation>();
        anim.Play("TileImpact");
    }
}
