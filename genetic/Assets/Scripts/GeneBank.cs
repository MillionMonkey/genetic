﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneBank {

    public Dictionary<int,Gene> genes = new Dictionary<int, Gene>();
    //Used to gain set of keys
    public List<int> tempKeys = new List<int>();
    public int counter;
    public System.Random random;

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addGene(Gene gene)
    {
        random = new System.Random();
        genes.Add(counter, gene);
        counter++;
    }

    public List<int> retrieveRandomKey(int numKeys)
    {
        int tempNumKeys = 0;
        tempKeys.Clear();
        while(tempNumKeys < numKeys)
        {
            int key = random.Next(0, genes.Count);
            if (!tempKeys.Contains(key))
            {
                tempKeys.Add(key);
                tempNumKeys++;
            }
        }
     
      return tempKeys;
   //     return Tuple.Create(genes[key], key);
    }

    public Gene retrieveGene(int key)
    {
        return genes[key];
        //     return Tuple.Create(genes[key], key);
    }


    public int getCount()
    {
        return counter;
    } 
}
