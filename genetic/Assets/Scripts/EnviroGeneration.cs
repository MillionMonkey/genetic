﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum TerrainType
{
    
    Plain1,
    Plain2,   
    Grassland1,
    Grassland2,
    Forest,
    Desert,
    Desolate,
    Plain3,
    Sea,
    Mountain
}


public enum ImpactType
{
    Meteor,
    Volcano
}

public enum Biomes
{
    Marsh,
    Sea,
    Temperate,
    Forest,
    GreatPlains,
    Mountains,
    Desert,
    Desolate
}



public class EnviroGeneration : MonoBehaviour {

    public List<GameObject> biomes = new List<GameObject>();
    public Dictionary<TerrainType, int> terrainChance = new Dictionary<TerrainType, int>();
    public GameObject[,] enviroGrid;
    public GameObject[,] speciesGrid;
    float ret;
    public GameObject tilePrefab;
    public GameObject biomePrefab;

    public Sprite sea;
    public Sprite grass1;
    public Sprite grass2;
    public Sprite grass3;
    public Sprite trees;
    public Sprite grass5;
    public Sprite grass6;
    public Sprite grass7;
    public Sprite grass8;
    public Sprite grass9;
    public Sprite grass10;
    public Sprite mountain;
    public int size = 512;
    public int biomeSize;
    public int mapSeed = 3;
    public int xOffset;
    public int yOffset;
    public int xTileSize;
    public int yTileSize;

    public int changeRate;
    public int humidity;
    public int temperature;

    public GUICore guiScript;

    public TerrainType[] tileSwitch;


    // Use this for initialization
    void Awake () {
        mapSeed = (int) UnityEngine.Random.Range(0, 10);
        enviroGrid = new GameObject[size, size];
        speciesGrid = new GameObject[size, size];
        tileSwitch = (TerrainType[])Enum.GetValues(typeof(TerrainType));
        terrainChanceSetUp();
        //    fillMap();
        //     fillMapBiomes();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void terrainChanceSetUp()
    {
        foreach (TerrainType foo in Enum.GetValues(typeof(TerrainType)))
        {
            terrainChance.Add(foo, 0);
        }
    }


    public void fillMapBiomes()
    {
        int numBiomes = (size / biomeSize);
        int i = 1000 * mapSeed;
        GameObject biome = null;
        float perlinSize = 5.0f;
        for (int y = 0; y < numBiomes; y++)
        {
            for (int x = 0; x < numBiomes; x++)
            {
                ret = Mathf.PerlinNoise((x + i) / perlinSize, (y + i) / perlinSize);
                biome = Instantiate(biomePrefab, new Vector3(x, y, 0), Quaternion.identity) as GameObject;
                biome.name = "Biome";
                

                int xStart = x * biomeSize;
                int yStart = y * biomeSize;
                Biomes type;

                if (ret < 0.5f)
                {
                    type = Biomes.Temperate;
                }
                else if (ret < 0.6f)
                {
                    type = Biomes.Marsh;
                }
                else
                {
                    type = Biomes.Forest;
                }
                biome.GetComponent<Biome>().setStats(biomeSize, mapSeed, this, guiScript);
                biome.GetComponent<Biome>().createTiles(type, xStart, yStart);
                biomes.Add(biome);
            }
        }
    }

    public void addTile(GameObject tile, int x, int y)
    {
        enviroGrid[x, y] = tile;
        speciesGrid[x, y] = tile;
    }


    //Calculates where a new group should be created at

    public Vector3 newEntity(GameObject group, Species species)
    {
        int x =size/2 + UnityEngine.Random.Range(-size / 3, size/3); ;
        int y = size / 2 + UnityEngine.Random.Range(-size / 3, size / 3); ;
        Vector3 returnVect = new Vector3(0,0,0);
        //     speciesGrid[x, y] != null &&
         returnVect = new Vector3(x, y, 0);
        speciesGrid[x, y] = group;
  //      else
  //      {
   //         Debug.Log("INVALID POSITION");
  //      }
       
        return returnVect;
    }

    public Vector3 split(GameObject group, Vector3 parent)
    {
        int x = UnityEngine.Random.Range(-2, 2) + (int)parent.x; 
        int y = UnityEngine.Random.Range(-2, 2) +(int) parent.y;

        Vector3 returnVect = new Vector3(x, y, 0);
        //     speciesGrid[x, y] != null &&
        if (MoveValid(x, y) == true)
        {
            speciesGrid[x, y] = group;
        }
        else
        {

            for (int i = 0; i < 9; i++)
            {
                x = (int)parent.x;
                y = (int)parent.y;

                if (i == 0) { x = x + 1; }
                if (i == 1) { x = x - 1; }
                if (i == 2) { y = y + 1; }
                if (i == 3) { y = y - 1; }
                if (i == 4) { x = x + 1; y = y + 1; }
                if (i == 5) { x = x - 1; y = y + 1; }
                if (i == 6) { x = x + 1; y = y - 1; }
                if (i == 7) { x = x - 1; y = y - 1; }
                if (MoveValid(x, y) == true)
                {
                    speciesGrid[x, y] = group;
                    returnVect = new Vector3(x, y, 0);
                    break;
                }

            }
        }


        return returnVect;
    }


    //Calculates where a group should move to
    public Vector3 getMove(int x, int y, GameObject group)
    {
        speciesGrid[x,y] = enviroGrid[x,y];
        int ranNum = 0;
        int origX = x;
        int origY = y;
        bool found = false;
        for (int i = 0; i < 9; i++)
        {

            ranNum = UnityEngine.Random.Range(0, 8);
            if (ranNum == 0) { x = x + 1; }
            else if (ranNum == 1) { x = x - 1; }
            else if (ranNum == 2) { y = y + 1; }
            else if (ranNum == 3) { y = y - 1; }
            else if (ranNum == 4) { x = x + 1; y = y + 1; }
            else if (ranNum == 5) { x = x - 1; y = y + 1; }
            else if (ranNum == 6) { x = x + 1; y = y - 1; }
            else if (ranNum == 7) { x = x - 1; y = y - 1; }
            if (MoveValid(x, y) == true)
            {
                speciesGrid[x, y] = group;
                i = 10;
                found = true;
            }

        }
        if(found == false)
        {
            x = origX;
            y = origY;
            speciesGrid[x, y] = group;
        }

        return new Vector3(x, y, 0);
    }

    public Vector3 manualMove(int x, int y, GameObject group, int moveInput)
    {
        speciesGrid[x, y] = enviroGrid[x, y];
        int origX = x;
        int origY = y;

            if (moveInput == 6) { x = x + 1; }
            else if (moveInput == 4) { x = x - 1; }
            else if (moveInput == 8) { y = y + 1; }
            else if (moveInput == 2) { y = y - 1; }
            else if (moveInput == 9) { x = x + 1; y = y + 1; }
            else if (moveInput == 7) { x = x - 1; y = y + 1; }
            else if (moveInput == 3) { x = x + 1; y = y - 1; }
            else if (moveInput == 1) { x = x - 1; y = y - 1; }
           if (MoveValid(x, y) == true)
            {
                speciesGrid[x, y] = group;
            }      
        else
        {
            x = origX;
            y = origY;
            speciesGrid[x, y] = group;
        }

        return new Vector3(x, y, 0);
    }

    public bool MoveValid(int x, int y)
    {
        //if speciesGrid != enviroGrid it means sGrid contains a group and not an enviro tile
        if (x>(size-1) || x < 0 || y>(size-1) || y<0)
        {
            return false;
        }
        if(speciesGrid[x, y] != enviroGrid[x,y])
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //Used by split to determine if a split is possible
    public bool AnyMoveValid(int x, int y)
    {
        if(!MoveValid(x+1,y) && !MoveValid(x, y+1) && !MoveValid(x -1, y) && !MoveValid(x, y-1) && !MoveValid(x + 1, y+1) && !MoveValid(x - 1, y-1) && !MoveValid(x + 1, y-1) && !MoveValid(x - 1, y + 1))
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public void impactTiles(ImpactType type, int x, int y, int dist, int impactSize)
    {
        if (bounds(x, y, 1, 0))
        {
            enviroGrid[x + 1, y].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, -1, 0))
        {
            enviroGrid[x - 1, y].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, 0, 1))
        {
            enviroGrid[x, y + 1].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, 1, 1))
        {
            enviroGrid[x + 1, y + 1].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, -1, 1))
        {
            enviroGrid[x - 1, y + 1].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, 0, -1))
        {
            enviroGrid[x, y - 1].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, -1, -1))
        {
            enviroGrid[x - 1, y - 1].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
        if (bounds(x, y, 1, -1))
        {
            enviroGrid[x + 1, y - 1].GetComponent<Terrain>().impact(type, dist + 1, impactSize);
        }
    }

    public int returnFood(int x, int y, int roamSize, Group group)
    {
        List<int> foodValues = new List<int>();
        foodValues.Add(enviroGrid[x, y].GetComponent<Terrain>().returnFood(group));
        if (bounds(x, y, 1, 0))
        {
            foodValues.Add(enviroGrid[x + 1, y].GetComponent<Terrain>().returnFood(group));
         }
        if (bounds(x, y, -1, 0))
        {
            foodValues.Add(enviroGrid[x - 1, y].GetComponent<Terrain>().returnFood(group));
        }
        if (bounds(x, y, 0, 1))
        {
            foodValues.Add(enviroGrid[x, y + 1].GetComponent<Terrain>().returnFood(group));
        }
        if (bounds(x, y, 1, 1))
        {
            foodValues.Add(enviroGrid[x + 1, y + 1].GetComponent<Terrain>().returnFood(group));
        }
        if (bounds(x, y, -1, 1))
        {
            foodValues.Add(enviroGrid[x - 1, y + 1].GetComponent<Terrain>().returnFood(group));
        }
        if (bounds(x, y, 0, -1))
        {
            foodValues.Add(enviroGrid[x, y - 1].GetComponent<Terrain>().returnFood(group));
        }
        if (bounds(x, y, -1, -1))
        {
            foodValues.Add(enviroGrid[x - 1, y - 1].GetComponent<Terrain>().returnFood(group));
        }
        if (bounds(x, y, 1, -1))
        {
            foodValues.Add(enviroGrid[x + 1, y - 1].GetComponent<Terrain>().returnFood(group));
        }

        int tilesFood = 0;
        for(int i = 0; i< foodValues.Count; i++)
        {
            tilesFood = tilesFood + foodValues[i];
        }

        return tilesFood;

    }

    public List<Group> groupsNearTerrain(int x, int y)
    {
        List<Group> retAdjGroups = new List<Group>();
        if (bounds(x,y,1,0))
        {
            if (speciesGrid[x + 1, y] != enviroGrid[x + 1, y]) { retAdjGroups.Add(speciesGrid[x + 1, y].GetComponent<Group>()); }
        }
        if (bounds(x, y, -1, 0))
        {
            if (speciesGrid[x - 1, y] != enviroGrid[x - 1, y]) { retAdjGroups.Add(speciesGrid[x - 1, y].GetComponent<Group>()); }
        }
        if (bounds(x, y, 0, 1))
        {
            if (speciesGrid[x, y + 1] != enviroGrid[x, y + 1]) { retAdjGroups.Add(speciesGrid[x, y + 1].GetComponent<Group>()); }
        }
        if (bounds(x, y, 1, 1))
        {
            if (speciesGrid[x + 1, y + 1] != enviroGrid[x + 1, y + 1]) { retAdjGroups.Add(speciesGrid[x + 1, y + 1].GetComponent<Group>()); }
        }
        if (bounds(x, y, -1, 1))
        {
            if (speciesGrid[x - 1, y + 1] != enviroGrid[x - 1, y + 1]) { retAdjGroups.Add(speciesGrid[x - 1, y + 1].GetComponent<Group>()); }
        }
        if (bounds(x, y, 0,-1))
        {
            if (speciesGrid[x, y - 1] != enviroGrid[x, y - 1]) { retAdjGroups.Add(speciesGrid[x, y - 1].GetComponent<Group>()); }
        }
        if (bounds(x, y, -1,-1))
        {
            if (speciesGrid[x - 1, y - 1] != enviroGrid[x - 1, y - 1]) { retAdjGroups.Add(speciesGrid[x - 1, y - 1].GetComponent<Group>()); }
        }
        if (bounds(x, y, 1, -1))
        {
            if (speciesGrid[x + 1, y - 1] != enviroGrid[x + 1, y - 1]) { retAdjGroups.Add(speciesGrid[x + 1, y - 1].GetComponent<Group>()); }
        }
        return retAdjGroups;
    }

    public void removeGroup(int x, int y)
    {
        speciesGrid[x, y] = enviroGrid[x, y];
    }

    //Unused
    public TerrainType getTerrainType(TerrainType type, bool random)
    {
        if (random == true)
        {
 //           TerrainType[] types = TerrainType.GetValues;
 //           int ran = UnityEngine.Random.Range(0, types.Length);
        }
        return type;
    }

    //Terrain changes gradually through ticks
    public void tick()
    {
        //Adjust individual Terrain

        int rate = UnityEngine.Random.Range(changeRate - changeRate/2, changeRate + changeRate/2);
        for (int i = 0; i < rate; i++)
        {
            int x = UnityEngine.Random.Range(0, size);
            int y = UnityEngine.Random.Range(0, size);
            int ranXd = 0;
            int ranYd = 0;
            int XorY = UnityEngine.Random.Range(0, 2);
            if (XorY == 1)
            {
               ranXd = UnityEngine.Random.Range(-1, 2);
            }
            else
            {
               ranYd = UnityEngine.Random.Range(-1, 2);
            }
            TerrainType type = enviroGrid[x, y].GetComponent<Terrain>().getType();
            if (bounds(x,y,ranXd,ranYd) && type != TerrainType.Mountain)
            {
                int basicChance = UnityEngine.Random.Range(0, 100);

                if (basicChance > 3)
                {
                    enviroGrid[x + ranXd, y + ranYd].GetComponent<Terrain>().setStats(type, null);
                }
                else
                {
                    //
                    List<int> temp = new List<int>(5);

                    //
                    TerrainType changedType = enviroGrid[x + ranXd, y + ranYd].GetComponent<Terrain>().getType();

                    float humidity = enviroGrid[x, y].GetComponent<Terrain>().biome.humidity;
                    float temperature = enviroGrid[x, y].GetComponent<Terrain>().biome.temperature;

                    int plainMod = 0;
                    int desertMod = 0;
                    int grasslandMod = 0;
                    int forestMod = 0;
                    int desolateMod = 0;

                    float tempMod;
                    float humidMod;
                    if (temperature < 20)
                    {
                        desolateMod = desolateMod + 2;
                        plainMod = plainMod + 10;
                        grasslandMod = grasslandMod + 5;
                        forestMod = forestMod + 5;
                    }
                    else
                    {
                        plainMod = plainMod + 5;
                        grasslandMod = grasslandMod + 10;
                    }
                    if (humidity < 45)
                    {
                        desolateMod = desolateMod + 2;
                        plainMod = plainMod + 30;
                        grasslandMod = grasslandMod + 15;
                        desertMod = desertMod + 10;
                    }
                    else
                    {
                        plainMod = plainMod + 10;
                        grasslandMod = grasslandMod + 15;
                        forestMod = forestMod + 10;
                    }
                    int total = plainMod + grasslandMod + forestMod + desertMod + desolateMod;
          //          Debug.Log(total);
                    if (plainMod > 0)
                    {
                        terrainChance[TerrainType.Plain1] = plainMod / 3;
                        terrainChance[TerrainType.Plain2] = plainMod / 3;
                        terrainChance[TerrainType.Plain3] = plainMod / 3;
                    }
                    if (forestMod > 0)
                    {
                        terrainChance[TerrainType.Forest] = forestMod;
                    }
                    if (desertMod > 0)
                    {
                        terrainChance[TerrainType.Desert] = desertMod;
                    }
                    if (grasslandMod > 0)
                    {
                        terrainChance[TerrainType.Grassland1] = grasslandMod / 2;
                        terrainChance[TerrainType.Grassland2] = grasslandMod / 2;
                    }
                    if (desolateMod > 0)
                    {
                        terrainChance[TerrainType.Desolate] = desolateMod;
                    }

          //          terrainChance[type] = terrainChance[type] + 1000;

                    int tempSum = 0;
                    int picked = 0;
                    float ranNum = UnityEngine.Random.Range(0, total);
                    for (int t = 0; t < tileSwitch.Length; t++)
                    {
                        tempSum = tempSum + terrainChance[tileSwitch[t]];
                        if (tempSum >= ranNum)
                        {
                            type = tileSwitch[t];
                            t = tileSwitch.Length;
                            //  Debug.Log(type);
                        }
                    }

                    if (changedType != TerrainType.Mountain)
                    {
                        enviroGrid[x + ranXd, y + ranYd].GetComponent<Terrain>().setStats(type, null);
                    }

                    terrainChance[TerrainType.Plain1] = 0;
                    terrainChance[TerrainType.Plain2] = 0;
                    terrainChance[TerrainType.Plain3] = 0;
                    terrainChance[TerrainType.Grassland1] = 0;
                    terrainChance[TerrainType.Grassland2] = 0;
                    terrainChance[TerrainType.Forest] = 0;
                    terrainChance[TerrainType.Desert] = 0;
                    terrainChance[TerrainType.Desolate] = 0;
                }
            }
        }


    }

    public bool bounds(int x, int y, int xD, int yD)
    {
        bool xValid = false;
        bool yValid = false;
        if (xD > 0)
        {
            if (x < (size - (1 + xD)))
            {
                xValid = true;
            }
        }
        if (xD < 0)
        {
            if (x > 0 + (xD + 1))
            {
                xValid = true;
            }
        }
        if (yD > 0)
        {
            if (y < (size - (1 + yD)))
            {
                yValid = true;
            }
        }
        if (yD < 0)
        {
            if (y > 0 + (yD + 1))
             {
                yValid = true;
            }
        }
        if(xD == 0)
        {
            xValid = true;
        }
        if(yD == 0)
        {
            yValid = true;
        }
        if (xValid && yValid)
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    public GameObject returnTile(int x, int y)
    {
        GameObject gridGroup = null;
        if (x < size && y < size)
        {
            gridGroup = speciesGrid[x, y];
        }

        return gridGroup;
    }

    public GameObject returnEnviroTile(int x, int y)
    {
        GameObject gridGroup = enviroGrid[x, y];

        return gridGroup;
    }
}
