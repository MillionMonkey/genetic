﻿using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;

public class Species : MonoBehaviour
{
    public char[] validCharacters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    public List<Creature> creatures;
    public List<Creature> maleFit = new List<Creature>();
    public List<Creature> femaleFit = new List<Creature>();
    public List<Drop> drops = new List<Drop>();
    public List<GameObject> groupList;
    public int numCreatures;
    public char[] origGene;
    public DarwinMachine core;
    public bool start;
    public int count = 3;
    public int numberOfGenes;
    public int groupCount = 1;
    public GameObject environment;
    public GameObject groupPrefab;
    public char[] origGenes;
    public int totalCreatures;

    public GUICore guiScript;

    public Sprite sprite;

    string ret;

    public bool basicSpecies;



    Species(int num, char[] origGene, DarwinMachine core,int count, int numberOfGenes)
    {
        this.count = count;
        this.origGene = origGene;
        this.numCreatures = num;
        this.core = core;
        this.numberOfGenes = numberOfGenes;
    }


    // Use this for initialization
    void Start () {
        char[] origGenes = new char[numberOfGenes];
        generateGroup();
    }

    public void startLife(bool basic)
    {
        basicSpecies = basic;
        creatures = new List<Creature>();
        maleFit = new List<Creature>();
        femaleFit = new List<Creature>();
  //      generateCreatures();
        start = true;
    }

    public void generateGroup()
    {
  //      createGenome();
        for (int i = 0; i < groupCount; i++)
        {

            GameObject group = Instantiate(groupPrefab, new Vector3(0,0,0), Quaternion.identity) as GameObject;
            Group groupScript = group.GetComponent<Group>();
            group.GetComponent<MouseOver>().guiScript = guiScript;
            groupScript.environment = environment;
            
            Vector3 location = environment.GetComponent<EnviroGeneration>().newEntity(group, this);
            group.transform.position = location;           
            groupScript.core = this;           
            groupScript.count = count;            
            groupScript.sprite = sprite;

            groupScript.speed = UnityEngine.Random.Range(1, 1000); ;
            groupScript.size = UnityEngine.Random.Range(1, 1000); ;
            groupScript.warmth = UnityEngine.Random.Range(1, 1000); ;
            groupScript.competitive = UnityEngine.Random.Range(1, 1000);
            groupScript.numCreatures = numCreatures;
            groupList.Add(group);
            if (basicSpecies == true)
            {
                group.name = "BGroup";
            }
            else
            {
                group.name = "AGroup";
                core.addPlayerGroup(group);
                groupScript.numCreatures = numCreatures;
                groupScript.origGene = origGenes;
                groupScript.numberOfGenes = numberOfGenes;
                groupScript.initalChromosomes();                
            }
            groupScript.startLife(basicSpecies);
        }
    }

    public void tick()
    {
        totalCreatures = 0;
        for (int i = 0; i < groupList.Count; i++)
        {
                groupList[i].GetComponent<Group>().tick();
        }
        for (int i = 0; i < groupList.Count; i++)
        {
            totalCreatures = totalCreatures + groupList[i].GetComponent<Group>().GetCreatureCount();
        }

    }

    public void addPlayerDrop(Drop drop)
    {
        drops.Add(drop);
    }

    public void move()
    {
        for (int i = 0; i < groupList.Count; i++)
        {
            groupList[i].GetComponent<Group>().move();
        }
    }

    public void split()
    {
        for (int i = 0; i < groupList.Count; i++)
        {
            groupList[i].GetComponent<Group>().Split();
        }
    }

    public void dead()
    {
        for (int i = 0; i < groupList.Count; i++)
        {
            groupList[i].GetComponent<Group>().Dead();
        }
    }

    public void removeGroup(GameObject group)
    {
        groupList.Remove(group);
    }

}
