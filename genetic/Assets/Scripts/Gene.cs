﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;




public class Gene {

    public GeneType type;
    public string name;
    //public List<Stat> stats = new List<Stat>();
    //public List<int> increase = new List<int>();
    public Stat stats;
    public int increase;
    public int maxAlleles;
    public System.Random random;


    public Gene(GeneType type, Stat stats, int increase)
    {
        this.type = type;
        this.stats = stats;
        this.increase = increase;
        random = new System.Random();

        string input = "A52M9D";
        char[] chars = input.ToArray();
        for (int i = 0; i < chars.Length; i++)
        {
            int randomIndex = random.Next(0, chars.Length);
            char temp = chars[randomIndex];
            chars[randomIndex] = chars[i];
            chars[i] = temp;
        }
        maxAlleles = random.Next(0, maxAlleles + 1);
        name = new string(chars);
    }


    public void runGene(int leftChromo, int rightChromo)
    {



    }

    public int mutateAllele()
    {

        return 0;
    }

    public int returnMaxAlleles()
    {
        return maxAlleles;
    }

    public GeneType getType()
    {
        return type;
    }
    public Stat getStat()
    {
        return stats;
    }
    public int getIncrease()
    {
        return increase;
    }
}
