﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FOV : MonoBehaviour {

    public List<GameObject> newFOV;
    public List<GameObject> previousFOV;
    public GameObject map;
    public EnviroGeneration enviroScript;
    public int visionRange;

    private void Start()
    {
        enviroScript = map.GetComponent<EnviroGeneration>();
    }

    public void fovNeighbours(List<GameObject> playerGroups, int mapsize)
    {
        int row_limit;
        int column_limit;
        for (int g = 0; g < playerGroups.Count; g++)
        {
            GameObject group = playerGroups[g];
            int fov = group.GetComponent<Group>().FOVRange;
            float gx = playerGroups[g].GetComponent<Group>().x;
            float gy = playerGroups[g].GetComponent<Group>().y;
            for (float x = Mathf.Max(0, gx - visionRange); x <= Mathf.Min(gx + visionRange, mapsize); x++)
            {
                for (float y = Mathf.Max(0, gy - visionRange); y <= Mathf.Min(gy + visionRange, mapsize); y++)
                {
                    GameObject tileFOV = enviroScript.returnTile((int)x, (int)y);
                       if (!newFOV.Contains(tileFOV) && tileFOV != null)
                        {
                    newFOV.Add(tileFOV);
                    }
                }
            }
            newFOV.Add(enviroScript.returnEnviroTile((int)gx, (int)gy));
        }

        displayFOV();
    }

    public void displayFOV()
    {
        var netFOV = newFOV.Except(previousFOV).ToList();
        var removeFOV = previousFOV.Except(newFOV).ToList();
        for (int x = 0; x < removeFOV.Count; x++)
        {
            //            previousFOV[x].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            //     previousFOV[x].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(80, 194, 88);
            if (removeFOV[x].transform.childCount > 0)
            {
                removeFOV[x].transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.gray;
            //   previousFOV[x].GetComponent<SpriteRenderer>().color = new Color(80, 194, 88);
            removeFOV[x].GetComponent<SpriteRenderer>().color = Color.gray;
            removeFOV[x].GetComponent<Terrain>().visible = false;
            }
            else
            {
                removeFOV[x].GetComponent<SpriteRenderer>().enabled = false;
            }
        }
        for (int x = 0; x < netFOV.Count; x++)
        {

            //           newFOV[x].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            //           newFOV[x].GetComponent<SpriteRenderer>().enabled = true;
            if (netFOV[x].transform.childCount > 0)
            {
                netFOV[x].transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.white;
                netFOV[x].GetComponent<SpriteRenderer>().color = Color.white;
                netFOV[x].GetComponent<Terrain>().visible = true;
                netFOV[x].GetComponent<Terrain>().show();
            }
            else
            {
                netFOV[x].GetComponent<SpriteRenderer>().enabled = true;
            }
        }
        previousFOV = new List<GameObject>(newFOV);
        newFOV.Clear();
        netFOV.Clear();
        removeFOV.Clear();


    }
}
