﻿using CielaSpike;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Profiling;

public class Group : MonoBehaviour {

    public char[] validCharacters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    public List<Creature> creatures;
    public List<Creature> lastGeneration;
    public List<Creature> splitGroup;
    public List<Creature> maleFit = new List<Creature>();
    public List<Creature> femaleFit = new List<Creature>();
    public int femaleFitSize;
    public List<string> debugGenes = new List<string>();
    public List<string> fitEvents = new List<string>();

    public Dictionary<int, Chromosome> chromosomesMap = new Dictionary<int, Chromosome>();

    public List<int> populationHistory;
    public int numDataPoints;

    public float speed;
    public float size;
    public float warmth;
    public float fertility;
    public float colour;
    public float competitive;
    public float children;
    public int FOVRange;

    public int movesTemp;
    public int movesCurrent;

    public int numCreatures;
    public char[] origGene;
    public Species core;
    public bool start;
    public int count = 3;
    public int numberOfGenes;
    public GameObject environment;
    public GameObject dropPrefab;
    public EnviroGeneration environmentScript;
    public Fitness fitness;
    public GameObject groupPrefab;
    public int creatureCount;
    public int food;
    public int numberInSplit;
    public bool split = false;
    public bool dead;

    public int generation;

    public List<Chromosome> startingChromosomes = new List<Chromosome>();
    public List<Chromosome> tempChromosomes = new List<Chromosome>();

    List<List<int>> tempChromoListInts = new List<List<int>>();
    List<List<int>> tempStatsListInts = new List<List<int>>();


    public List<int> speedKeys;
    public List<int> colorKeys;
    public List<int> sizeKeys;
    public List<int> warmthKeys;
    public List<int> competitiveKeys;
    public List<int> fertilityKeys;
    public List<int> healthKeys;

    public int childFail;
    public int reproductionFail;
    public int adultFail;

    //Set in inspector
    public int chromosomePoolCount;
    public int chromosomeCount;

    public int dCount = 0;

    int totalChromo;


    public Sprite sprite;

    string ret;

    public int bugCount;

    //Core stats
    public int gSize = 50;

    public bool first = false;

    public float debugSize;

    //Basic group variables
    public bool basic = true;
    public bool done;
    public int basicCreatures;

    public float x;
    public float y;

    public CreatureSpool spool;
    public System.Random random;

 //   public static Task task1;
 //   public static Task task2;

    public static Coroutine thread1;
    public static Coroutine thread2;

    public bool done1;
    public bool done2;

    public int leftOrRight;

    // Use this for initialization
    void Start()
    {
        //char[] origGenes = new char[numberOfGenes];
        //environmentScript = environment.GetComponent<EnviroGeneration>();
        //split = false;
        //competitive = (int)random.Range(65, 89);
    }

    public void startLife(bool basic)
    {
        this.basic = basic;
        random = new System.Random();
         gameObject.GetComponent<SpriteRenderer>().sprite = sprite;        
        environmentScript = environment.GetComponent<EnviroGeneration>();
        split = false;
        movesCurrent = 1;
        generation = 0;
        numDataPoints = 10;
        movesTemp = movesCurrent;

        for(int i = 0; i< numDataPoints; i ++)
        {
            populationHistory.Add(0);
        }

        competitive = random.Next(65,89);
        tileCalculation();
        if (basic == false)
        {
            creatures = new List<Creature>();
            maleFit = new List<Creature>();
            femaleFit = new List<Creature>();
            splitGroup = new List<Creature>();
            speedKeys = new List<int>();
            colorKeys = new List<int>();
            sizeKeys = new List<int>();
            warmthKeys = new List<int>();
            competitiveKeys = new List<int>();
            fertilityKeys = new List<int>();
            healthKeys = new List<int>();
            fitness = core.core.GetComponent<Fitness>();
            generateCreatures();
        }
        else
        {
            basicCreatures = random.Next(100, 500);
        }
        start = true;
    }

    public void splitLife(List<Creature> splitCreatures)
    {
        creatures = new List<Creature>();
        maleFit = new List<Creature>();
        femaleFit = new List<Creature>();
        splitGroup = new List<Creature>();
        for (int i  = 0;i < splitCreatures.Count;i++)
        {
            creatures.Add(splitCreatures[i]);
            splitCreatures[i].split(this);
        }
        start = true;
    }

    public void initalChromosomes()
    {
        ChromosomeSpool spool = core.core.spools.GetComponent<ChromosomeSpool>();
        startingChromosomes = new List<Chromosome>();
        tempChromosomes = new List<Chromosome>();
        for (int i = 0; i < chromosomePoolCount; i++)
        {
            //      Chromosome chromosome = new Chromosome(core.core.geneGenerator, 10,i);
            Chromosome chromosome = spool.returnChromosome();
            chromosome.loadInformationDataInitial(core.core.geneGenerator, 100, i);
            startingChromosomes.Add(chromosome);
            chromosomesMap.Add(chromosome.number, chromosome);
        }
    }


    public void generateCreatures()
    {
        first = true;
        chromosomeCount = chromosomePoolCount;
        CreatureSpool spool = core.core.spools.GetComponent<CreatureSpool>();
        Creature creature;
        for (int i = 0; i < numCreatures; i++)
        {
            //     Creature creature = new Creature(numberOfGenes, this);
            List<List<int>> chromosomeList = tempChromoList();
            List<List<int>> statsList = tempStatsList();
            //Creature creature = new Creature(size, speed, competitive, warmth, tempChromosomes, this);
            //creatures.Add(creature);
            creature = spool.returnNextCreature();

            creature.loadInitial(chromosomeList, statsList, this);
           creatures.Add(creature);
            for (int t = 0; t < creature.chromosomes.Count; t++)
            {
     //           Debug.Log(creature.chromosomes[t].genes.Count);
            }
        }

 //       ret = creatures[0].getGenes();

 //       Debug.Log(ret);
    }

    public List<List<int>> tempChromoList()
    {
        List<int> tempList = new List<int>();
        List<List<int>> tempChromoListInts = new List<List<int>>();
        for (int i = 0; i < chromosomeCount; i = i + 2)
        {
            //      int index = random.Next(0, chromosomePoolCount);
            //     tempChromosomes.Add(startingChromosomes[i]);
            //     tempChromosomes.Add(startingChromosomes[i]);
            tempList.Add(startingChromosomes[i].number);
            for (int o = 0; o < startingChromosomes[i].genes.Count; o++)
            {
                tempList.Add(random.Next(0, 100));
            }
            tempChromoListInts.Add(tempList);
            tempList.Clear();
            tempList.Add(startingChromosomes[i].number);
            for (int o = 0; o < startingChromosomes[i].genes.Count; o++)
            {
                tempList.Add(random.Next(0, 100));
            }
            tempChromoListInts.Add(tempList);
        }
       return tempChromoListInts;
    }

    public List<List<int>> tempStatsList()
    {
        List<int> tempList = new List<int>();
        List<List<int>> tempChromoListInts = new List<List<int>>();
        for (int i = 0; i < chromosomeCount; i = i + 2)
        {
            startingChromosomes[i].calculateBaseStats();
            tempList.Add(startingChromosomes[i].number);
            for (int o = 0; o < startingChromosomes[i].stats.Count; o++)
            {
                tempList.Add(startingChromosomes[i].stats[o]);
            }
            tempChromoListInts.Add(tempList);
            tempList.Clear();
            tempList.Add(startingChromosomes[i].number);
            for (int o = 0; o < startingChromosomes[i].stats.Count; o++)
            {
                tempList.Add(startingChromosomes[i].stats[o]);
            }
            tempChromoListInts.Add(tempList);
        }
        return tempChromoListInts;
    }

    public void nextGen()
    {
        float newSize;
        float ranAdjust;
        generation++;
        spool = core.core.spools.GetComponent<CreatureSpool>();
        if (basic == false)
        {
            lastGeneration = new List<Creature>(creatures);
            creatures.Clear();

            StartCoroutine(generateChildrenThread(0, 0));

            //creatures.Clear();
            //creatures.AddRange(spool.GetComponent<CreatureSpool>().returnCreaturesUsed());

            maleFit.Clear();
            femaleFit.Clear();
            //    debugGenes.Clear();
            bugCount = 0;
            if (creatures.Count > 0)
            {
                debugSize = creatures[0].size;
            }
            for (int i = 0; i < creatures.Count; i++)
            {
                fitnessFunction(creatures[i]);
            }
     //       Debug.Log("m " + maleFit.Count + " f " + femaleFit.Count);
        }
        else
        {
            if (food > (basicCreatures * 20))
            {
                gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                basicCreatures = basicCreatures + random.Next(-5,20);
            }
            else
            {
                basicCreatures = basicCreatures + random.Next(-15,5);
            }
        }
        if (food > (creatureCount * 20))
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }

        //Purely for debugging
        if(basic == false) { creatureCount = creatures.Count; }


        first = false;
    }

    IEnumerator generateChildrenThread(int numChildren, int indexS)
    {
   //     Debug.Log("1 " + creatureCount);
        int totalChildren = femaleFit.Count * 4;
        int totalChromo = totalChildren * chromosomeCount;
        int totalThreads = 4;
        femaleFitSize = femaleFit.Count;
        int femaleChunk = femaleFit.Count / totalThreads;
        int childrenChunk = totalChildren / totalThreads;
        int chromoChunk = totalChromo / totalThreads;
        Task task1;
        Task task2;
        Task task3;
        Task task4;
        done1 = false;
        done2 = false;
        done = false;
  //      Debug.Log("Total " + totalChromo);
        this.StartCoroutineAsync(GenerateChildren(1, femaleChunk * 0, femaleChunk * 1, childrenChunk * 0, childrenChunk * 1, chromoChunk * 0, chromoChunk * 1), out task1);
   //     Debug.Log("Thread1 " + "Start");
        this.StartCoroutineAsync(GenerateChildren(2, femaleChunk * 1, femaleChunk * 2, childrenChunk * 1, childrenChunk * 2, chromoChunk * 1, totalChromo * 2), out task2);
        this.StartCoroutineAsync(GenerateChildren(3, femaleChunk * 2, femaleChunk * 3, childrenChunk * 2, childrenChunk * 3, chromoChunk * 2, chromoChunk * 3), out task3);
        this.StartCoroutineAsync(GenerateChildren(4, femaleChunk * 3, femaleChunk * 4, childrenChunk * 3, childrenChunk * 4, chromoChunk * 3, chromoChunk * 4), out task4);
        //      Debug.Log("Thread2 " + "Start");

        while (task1.State != TaskState.Done || task2.State != TaskState.Done || task3.State != TaskState.Done || task4.State != TaskState.Done)
        {
            if (task1.State == TaskState.Error || task2.State == TaskState.Error || task3.State == TaskState.Error || task4.State == TaskState.Error)
            {
                Debug.Log("CRITICAL ERROR IN THREAD");
                yield break;
            }
        }
        Debug.Log("2 " + creatureCount);
        yield break;
        //     yield return this.StartCoroutineAsync(GenerateChildren((femaleFit.Count / totalThreads) + 1, femaleFit.Count, totalChildren / totalThreads + 1, numChildren), out task2);

    }

    IEnumerator GenerateChildren(int tNum,int indexStart, int indexEnd, int indexCreatureStart,int indexCreatureEnd, int indexChromosomeStart, int indexChromosomeEnd)
    {
        int cIndex = 0;
        int chromoIndex = indexChromosomeStart;
        int creatureIndex = indexCreatureStart;
     //   ThreadRandom randomForThread = new ThreadRandom();

        for (int i = indexStart; i < indexEnd; i++)
        {

            for (int l = 0; l < 4; l++)
            {
                if (maleFit.Count > 0 && femaleFit.Count > 0)
                {
                    Creature selectedMale = maleFit[ThreadRandom.Next(0,maleFit.Count)];
                    try
                    {                      
                        Creature child = spool.returnNextCreatureThread(creatureIndex);
                        lock (creatures)
                        {
                            creatures.Add(child);
                        }
                        child.clearChromosomes();
                        child.addParent(selectedMale, femaleFit[i]);
                        loadParentGenetics(child,chromoIndex);
                        child.loadNextGen(this, generation);
                        child.mutate();
       //                 chromoIndex = chromoIndex + chromosomeCount; // Problem area
                //        Debug.Log(chromoIndex + " " + Thread.CurrentThread.ManagedThreadId);
                        creatureIndex++;
                    }
                    catch(System.IndexOutOfRangeException e)
                    {
                        Debug.Log(e + " LeftorRight " + leftOrRight);
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(e);
                    }
                }
            }
 //           femaleFit[i].clearData();
        }
 //       Debug.Log("Thread " + "End Actual");
        if(tNum == 1)
        {
            done1 = true;
        }
        if(tNum == 2)
        {
            done2 = true;
        }

        yield break;
    }

    public void loadParentGenetics(Creature child, int spoolIndex)
    {
       
        for (int i = 0; i< chromosomeCount;i = i+2)
        {

            try
            { //chromosomes are stored[1f,1m,2f,2m...]
              //Note only the index could be stored until the fitness function runs for efficenty
                leftOrRight = i / 2 + ThreadRandom.Next(0, 2);
   //             Debug.Log(ThreadRandom.Next(0, 2));
                child.addChromosomeList(child.father.chromosomesAlleles[leftOrRight], child.father.chromosomesStats[leftOrRight]);
          //      child.addChromosome(child.father.chromosomes[leftOrRight], spoolIndex+i);
                leftOrRight = i / 2 + ThreadRandom.Next(0, 2);
                child.addChromosomeList(child.mother.chromosomesAlleles[leftOrRight], child.mother.chromosomesStats[leftOrRight]);
            }
            catch(System.Exception e)
            {
                Debug.Log(e);
             
            }
        }


    }

    public void failCount(StageFailed stage)
    {
        if(stage == StageFailed.Childhood)
        {
            childFail++;
        }
        else if(stage == StageFailed.Adulthood)
        {
            adultFail++;
        }
        else if (stage == StageFailed.Reproduction)
        {
            reproductionFail++;
        }
    }

    public void Split()
    {
        if (split == true)
        {
            split = false;
            GameObject group = Instantiate(groupPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            group.name = this.gameObject.name;
            Group groupScript = group.GetComponent<Group>();
            core.core.addPlayerGroup(group);

            group.transform.position = gameObject.transform.position;
            Vector3 location = environmentScript.split(group, gameObject.transform.position);
            group.GetComponent<Group>().splitMove(location);


            if (basic == false)
            {
                //Move 100 creatures into new Group - NOTE THIS IS NOT PERFECTLY RANDOM, BIAS EXISTS BASED ON LIST ORDER
                int total = 0;

                for (int i = 0; i < creatures.Count; i++)
                {
                    float nom = (numberInSplit - total);
                    float deNom = (creatures.Count - i);
                    float ranCalc = nom / deNom;
                    if (random.Next(0, 2) < ranCalc)
                    {
                        total++;
                        splitGroup.Add(creatures[i]);
                        creatures.RemoveAt(i);
                        if (total == numberInSplit)
                        {
                            break;
                        }
                    }
                }
                groupScript.numCreatures = numCreatures;
                groupScript.core = core;
                groupScript.origGene = origGene;
                groupScript.count = count;
                groupScript.numberOfGenes = numberOfGenes;
                groupScript.splitLife(splitGroup);
                splitGroup.Clear();
            }
            else
            {
                groupScript.basicCreatures = basicCreatures / 2;
                basicCreatures = basicCreatures / 2;
            }
            groupScript.speed = speed;
            groupScript.size = size;
            groupScript.warmth = warmth;
            groupScript.competitive = competitive;
            core.GetComponent<Species>().groupList.Add(group);

            }     
    }

        //Runs for every creature
    public bool fitnessFunction(Creature creature)
    {
        creature.loadAllGenes();
        bool fit = fitness.fitnessTest(creature, creatureCount, food);
        if(fit == false)
        {
            creature.dead = true;
  //          creature.clearParent();
            return false;
        }        
        else
        {
            string gender = "";
            float chance = (float)ThreadRandom.NextDouble() * (1 - 0) + 0;
   //         Debug.Log(chance);
            if (chance > 0.4f)
            {
                gender = "male";
            }
            else
            {
                gender = "female";
            }
            addFit(creature, gender);
   //         creature.clearParent();
            return true;
        }

    }

    
    public void splitDrop()
    {
        GameObject drop = Instantiate(dropPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        GameObject tile = environmentScript.returnEnviroTile((int)x, (int)y);
        drop.transform.parent = tile.transform;
        //      drop dropScript = drop.GetComponent<drop>();
        Drop dropScript = drop.GetComponent<Drop>();
        core.addPlayerDrop(dropScript);

        drop.transform.position = tile.transform.position;
        //Vector3 location = environmentScript.split(drop, gameObject.transform.position);
        ////Move 100 creatures into new Group - NOTE THIS IS NOT PERFECTLY RANDOM, BIAS EXISTS BASED ON LIST ORDER
        //int total = 0;
        int numberInDrop = creatures.Count / 2;
        for (int i = 0; i < numberInDrop; i++)
        {
            List<Creature> tempList = new List<Creature>();
            tempList.Add(creatures[i]);
            creatures.RemoveAt(i);
        //    float nom = (numberInSplit - total);
        //    float deNom = (creatures.Count - i);
        //    float ranCalc = nom / deNom;
        //    if (random.Range(0, 2) < ranCalc)
        //    {
        //        total++;
        //        splitGroup.Add(creatures[i]);
        //        creatures.RemoveAt(i);
        //        if (total == numberInSplit)
        //        {
        //            break;
        //        }
        //    }
        }
    }

    //Runs once per generation, sets up fitness function
    public void determineFitEvents()
    {
        // Determine quantity of food available.


    }

    public void addFit(Creature creature, string gender)
    {
        if (gender == "male")
        {
            maleFit.Add(creature);
        }
        if (gender == "female")
        {
            femaleFit.Add(creature);
        }
   //     Debug.Log(maleFit.Count + " " + femaleFit.Count);
        //if(creature.chromosomes.Count != 10)
        //{
        //    Debug.Log("fit C " + creature.chromosomes.Count);
        //}
        
    }

    public void tick()
    {
        speed = 0;
        adultFail = 0;
        childFail = 0;
        reproductionFail = 0;
        int numCreatures = 0;
        if (basic == false)
        { 
            populationHistory.Add(creatures.Count);
        populationHistory.RemoveAt(0);
        numCreatures = creatures.Count;}
        else{numCreatures = basicCreatures;}
            if (numCreatures > 0)
            {
                tileCalculation();
                determineFitEvents();
                nextGen();
                if (numCreatures > numberInSplit * 2)
                {
                    split = true;
                }
            }
            else if (numCreatures <= 0)
            {
                dead = true;
            }
        if (basic == false)
        {
            calculateTotals();
        }
    }

    public void calculateTotals()
    {
        for (int i = 0; i < creatures.Count; i++)
        {
            speed = speed + creatures[i].speed;
            size = size + creatures[i].size;
            fertility = fertility + creatures[i].fertility;
            warmth = warmth + creatures[i].warmth;
            colour = colour + creatures[i].color;
        }
        speed = speed / creatures.Count;
        size = size / creatures.Count;
        fertility = fertility / creatures.Count;
        warmth = warmth / creatures.Count;
        colour = colour / creatures.Count;
    }

    public void tileCalculation()
    {
       food = environmentScript.returnFood((int)transform.position.x,(int) transform.position.y, 9, this);
    }

    public void move()
    {
        for (int i = 0; i < movesTemp; i++)
        {
            Vector3 location = environmentScript.getMove((int)transform.position.x, (int)transform.position.y, this.gameObject);
            x = location.x;
            y = location.y;
            StartCoroutine(MoveOverSeconds(gameObject, new Vector3(location.x, location.y, 0f), core.core.tickSpeed / 2));
            //      gameObject.transform.position = location;
        }

        movesTemp = movesCurrent;
    }
    

    public void manualMove(int inputMove)
    {
        Vector3 location = environmentScript.manualMove((int)transform.position.x, (int)transform.position.y, this.gameObject,inputMove);
        x = location.x;
        y = location.y;
        StartCoroutine(MoveOverSeconds(gameObject, new Vector3(location.x, location.y, 0f), core.core.tickSpeed / 2));
        movesTemp--;
    }

    public void splitMove(Vector3 location)
    {
        StartCoroutine(MoveOverSeconds(gameObject, new Vector3(location.x, location.y, 0f), core.core.tickSpeed / 2));
    }

    public void Dead()
    {
        if (dead == true)
        {
            core.removeGroup(this.gameObject);
            environmentScript.removeGroup((int) transform.position.x, (int)transform.position.y);
            core.core.removePlayerGroup(this.gameObject);
            Destroy(gameObject);
        }
    }

    public int GetCreatureCount()
    {
        int count = 0;
        if(basic == false)
        {
            count = creatures.Count;
        }

        return count;
    }

    public string getDetails()
    {
        string details;
        if (basic == true)
        {
            details = "Number: " +basicCreatures + "\n" + "Size: " + size;
        }
        else
        {
            details = "Number: " + creatures.Count;
        }
        return details;
    }

    public IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = end;
    }
}

internal class ThreadManager
{
}