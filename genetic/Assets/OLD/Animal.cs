using System;

public class Animal<T>
{
	public T[] genes { get; private set; }
	public float fitness { get; private set; }

	private Random random;
	private Func<T> getRandomGene;
	private Func<int, float> fitnessFunction;

	public Animal(int size, Random random, Func<T> getRandomGene, Func<int, float> fitnessFunction, bool shouldInitGenes = true)
	{
		genes = new T[size];
		this.random = random;
		this.getRandomGene = getRandomGene;
		this.fitnessFunction = fitnessFunction;

		if (shouldInitGenes)
		{
			for (int i = 0; i < genes.Length; i++)
			{
				genes[i] = getRandomGene();
			}
		}
	}

	public float CalculateFitness(int index)
	{
		fitness = fitnessFunction(index);
		return fitness;
	}

	public Animal<T> Crossover(Animal<T> otherParent)
	{
		Animal<T> child = new Animal<T>(genes.Length, random, getRandomGene, fitnessFunction, shouldInitGenes: false);

		for (int i = 0; i < genes.Length; i++)
		{
			child.genes[i] = random.NextDouble() < 0.5 ? genes[i] : otherParent.genes[i];
		}

		return child;
	}

	public void Mutate(float mutationRate)
	{
		for (int i = 0; i < genes.Length; i++)
		{
			if (random.NextDouble() < mutationRate)
			{
				genes[i] = getRandomGene();
			}
		}
	}
}