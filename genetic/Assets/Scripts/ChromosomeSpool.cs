﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ChromosomeSpool : MonoBehaviour
{
    public List<Chromosome> chromosomesAvailable1 = new List<Chromosome>();
    public List<Chromosome> chromosomesAvailable2 = new List<Chromosome>();
    public int numInitialChromosomes;
    public int extraChromosomesNum;
    public int totalChromosomes;
    public int indexUpTo;
    public int listToUse;
    public int lastGenIndex;
    public List<Chromosome> chromosomes = new List<Chromosome>();

    public List<int> indexDebug = new List<int>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    public void initialChromosomeGeneration()
    {
        indexUpTo = 0;
        chromosomeGeneration(numInitialChromosomes);
        listToUse = 1;
    }

    public void bulkChromosomeGeneration()
    {
        chromosomeGeneration(extraChromosomesNum);
    }

    public void chromosomeGeneration(int num)
    {
        for (int i = 0; i < num; i++)
        {
            Chromosome chromosome = new Chromosome();
            chromosomesAvailable1.Add(chromosome);
            chromosome = new Chromosome();
            chromosomesAvailable2.Add(chromosome);
        }
        totalChromosomes = totalChromosomes + num;

    }

    public Chromosome returnChromosome()
    {
        Chromosome creatureRet = null;
        if (listToUse == 1)
        {
            if (indexUpTo >= chromosomesAvailable1.Count)
            {
                bulkChromosomeGeneration();
            }
            creatureRet = chromosomesAvailable1[indexUpTo];
            //        Debug.Log(1);
        }
        else
        {
            if (indexUpTo >= chromosomesAvailable2.Count)
            {
                bulkChromosomeGeneration();
            }
            creatureRet = chromosomesAvailable2[indexUpTo];
            //         Debug.Log(2);
        }

        indexUpTo++;
        return creatureRet;
    }

    public Chromosome returnNextChromosomeThread(int index)
    {
        //if(indexDebug.Contains(index))
        //{
        //    Debug.Log("Double up " + index);
        //}
        //else
        //{
        //    indexDebug.Add(index);
        //}
   //     Debug.Log(index + " " + Thread.CurrentThread.ManagedThreadId);
        try {
            Chromosome creatureRet = null;
            if (listToUse == 1)
            {
                if (index >= chromosomesAvailable1.Count)
                {
                    bulkChromosomeGeneration();
                }
                creatureRet = chromosomesAvailable1[index];
                //        Debug.Log(1);
            }
            else
            {
                if (index >= chromosomesAvailable2.Count)
                {
                    bulkChromosomeGeneration();
                }
                creatureRet = chromosomesAvailable2[index];
                //         Debug.Log(2);
            }
            indexUpTo++;
     //       Debug.Log("chromosomeSpool" + index);
            return creatureRet;
    }
        catch (System.Exception e)
        {
            Debug.Log("chromo get" + e);
            return null;
        }
    }


    public void endTick()
    {
  //      indexDebug.Clear();
        lastGenIndex = indexUpTo;
        if (listToUse == 1)
        {
            listToUse = 2;
        }
        else if (listToUse == 2)
        {
            listToUse = 1;
        }
        indexUpTo = 0;
    }
}
