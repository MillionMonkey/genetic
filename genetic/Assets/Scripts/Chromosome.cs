﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Profiling;

public class Chromosome {

 //   public Dictionary<int, Gene> genes = new Dictionary<int, Gene>();
 //   public Dictionary<int, int> alleles = new Dictionary<int, int>();
  //  public Dictionary<int, int> allelesDominant = new Dictionary<int, int>();
 //   public Dictionary<int, bool> dominant = new Dictionary<int, bool>();
    public GeneBank bank;
    public GeneGeneration geneGeneration;
    public int numGenes = 50;
    public GameObject group;
    public System.Random random;

    public List<Gene> genes = new List<Gene>();
    public List<int> alleles = new List<int>();
    public List<Gene> specialGenes = new List<Gene>();

    public List<int> stats = new List<int>(); // Stats below but in list, same order

    public int speed;
    public int color;
    public int size;
    public int warmth;
    public int competitive;
    public int fertility;
    public int health;

    public int number;

    public int index;


    public Chromosome(GeneGeneration geneGeneration, int numGenes, int number)
    {
        bank = geneGeneration.bank;
        random = new System.Random();
        genes = new List<Gene>();
        alleles = new List<int>();
 //       dominant = new Dictionary<int, bool>();
        this.numGenes = numGenes;
        this.number = number;
   //     fillGenes();
    }

    public Chromosome()
    {
        genes = new List<Gene>();
        random = new System.Random();
        alleles = new List<int>();
    }

    public void loadInformationDataInitial(GeneGeneration geneGeneration, int numGenes, int number)
    {
        bank = geneGeneration.bank;
        this.numGenes = numGenes;
        this.number = number;
        fillGenes();
        initialMutate();
    }

    public void loadInformationNextGen(List<Gene> genesIn, List<int> allelesIn, int number, int size, int warmth, int competitive, int speed, int fertility, int color)
    {
        int tempCount = genesIn.Count;
        try {
                //   Profiler.BeginSample("gene list1");
                this.genes.Clear();
                this.genes.AddRange(genesIn);
        }
        catch (System.Exception e)
        {
            Debug.Log("chromo load1" + e);

        }
        //   Profiler.EndSample();
        //Profiler.BeginSample("gene list1.2");
        //this.genes.InsertRange(0,genes);        ////Profiler.EndSample();
        //Profiler.EndSample();
        //Profiler.BeginSample("gene list2");
        //      this.genes = new List<Gene>(genes);
        //Profiler.EndSample();
    
        //Profiler.BeginSample("allele list1");
        //this.alleles = new List<int>(alleles);
        //Profiler.EndSample();
        try { 
        Profiler.BeginSample("allele list1");
                this.alleles.Clear();
                this.alleles.AddRange(allelesIn);
    
        Profiler.EndSample();
        Profiler.BeginSample("stats");

    }
        catch (System.Exception e)
        {
            Debug.Log("chromo load2" + e);

        }

        this.size = size;
        this.warmth = warmth;
        this.competitive = competitive;
        this.speed = speed;
        this.fertility = fertility;
        this.color = color;
        this.number = number;

        Profiler.EndSample();
        Profiler.BeginSample("mutate");
            mutate();
        Profiler.EndSample();



    }

    public void fillGenes()
    {      
        List<int> keys = bank.retrieveRandomKey(numGenes);
        for (int i = 0; i < keys.Count; i++)
        {
            int key = keys[i];
            Gene gene = bank.retrieveGene(key);
            genes.Add(gene);
            alleles.Add(0);
 //           int allele = (int)UnityEngine.Random.Range(0, 10);
 //           alleles.Add(allele);
            int dom = random.Next(0, 2);
    //        allelesDominant.Add(key, dom);

            //Index genes by stats for faster lookup for the fitness function
            Stat type = gene.stats;
            //NOTE TO DO: Change to contains when we have a list instead of single
            if (type == Stat.Speed) { speed = speed + gene.increase; }
            else if (type == Stat.Color) {color = color + gene.increase; }
            else if(type == Stat.Size) { size = size + gene.increase; }
            else if (type == Stat.Warmth) { warmth = warmth + gene.increase; }
            else if(type == Stat.Competitive) { competitive = competitive + gene.increase; }
            else if(type == Stat.Fertility) {  fertility = fertility + gene.increase; }
      //      if (type == Stat.Health) { healthKeys.Add(key); health = health + gene.increase; }
        }
 //       Debug.Log(size + " " + color + " " + speed + " " + fertility + " |");
    }

    public void mutate()
    {
        try { 
        int allele = random.Next(-2, 3);
        index = random.Next(0, genes.Count);
        try
        {
            alleles[index] = allele + alleles[index];
        }
        catch (System.Exception e)
        {
            Debug.Log(" alleles " + index + " Size " + alleles.Count + e);
        }
        Gene gene = genes[index];
        Stat type = genes[index].stats;

        if (type == Stat.Speed) { speed = speed +allele; }
        else if(type == Stat.Color) { color = color + allele; }
        else if(type == Stat.Size) { size = size + allele; }
        else if(type == Stat.Warmth) { warmth = warmth + allele; }
        else if(type == Stat.Competitive) { competitive = competitive + allele; }
        else if(type == Stat.Fertility) { fertility = fertility + allele; }
    }
            catch(System.Exception e)
            {
                Debug.Log("chromo mutate " + genes.Count + " " + index + " " + e);
             
            }
    
    }

    public void calculateBaseStats()
    {
        stats.Add(random.Next(-100, 100));
        stats.Add(random.Next(-100, 100));
        stats.Add(random.Next(-100, 100));
        stats.Add(random.Next(-100, 100));
        stats.Add(random.Next(-100, 100));
        stats.Add(random.Next(-100, 100));
        //speed = random.Next(-100,100); 
        //color = random.Next(-100, 100); 
        //size = random.Next(-100, 100); 
        //warmth = random.Next(-100, 100); 
        //competitive = random.Next(-100, 100); 
        //fertility = random.Next(-100, 100); 
    }

    public void initialMutate()
    {
        for(int i = 0; i < 100; i++)
        try
        {
            int allele = random.Next(-2, 3);
            index = random.Next(0, genes.Count);
            try
            {
                alleles[index] = allele + alleles[index];
            }
            catch (System.Exception e)
            {
                Debug.Log(" alleles " + index + " Size " + alleles.Count);
            }
            Gene gene = genes[index];
            Stat type = genes[index].stats;

            if (type == Stat.Speed) { speed = speed + allele; }
            else if (type == Stat.Color) { color = color + allele; }
            else if (type == Stat.Size) { size = size + allele; }
            else if (type == Stat.Warmth) { warmth = warmth + allele; }
            else if (type == Stat.Competitive) { competitive = competitive + allele; }
            else if (type == Stat.Fertility) { fertility = fertility + allele; }
        }
        catch (System.Exception e)
        {
            Debug.Log("chromo mutate " + genes.Count + " " + index);

        }
    }

    public int returnAlleleMutate(int origAllele, int position)
    {
        int allele = genes[position].mutateAllele(origAllele);
        return allele;
    }

    public void clearData()
    {
        genes.Clear();
        alleles.Clear();
    }
}
