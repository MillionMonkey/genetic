﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum WindowUpdating
{
    Nil,
    CurrentGenGroup,
    PreviousGenGroup,
  //  Chromosome,
  //  Genes
}

public class GUICore : MonoBehaviour {

    public GameObject canvas;

    public Sprite ftSuccess;
    public Sprite ftChildhood;
    public Sprite ftAdult;
    public Sprite ftReproduction;
    public Sprite ftAlive;

    public float screenWidth;
    public float screenHeight;

    public Text tickCount;
    public Text totalCreatures;

    public WindowUpdating updateWindow;

    //Fit panel
    public GameObject fitMainPanel;
    public GameObject fitCreature;

    //Creature Panel;
    public GameObject creaturePanel;
    public Image cPmainImage;
    public Image cPMother;
    public Image cPFather;
    public Image cPFailPoint;
    public Creature selectedCreature;
    public Text speed;
    public Text size;
    public Text warmth;
    public Text color;
    public Text competitive;
    public Text fertility;

    //Chromosome Panel
    public List<Chromosome> chromosomes = new List<Chromosome>();
    public GameObject chromosomePanel;
    public List<GameObject> chromosomeGroups = new List<GameObject>();
    public GameObject chromoGroup;

    public GameObject tooltipPanel;
    public Text cSpeed;
    public Text cSize;
    public Text cWarmth;
    public Text cCompetitive;
    public Text cFertility;
    public Text cNumber;

    public Slider speedSlider;
    public Text speedSliderValue;
    public Slider fertilitySlider;
    public Text fertilitySliderValue;
    public Slider sizeSlider;
    public Text sizeSliderValue;
    public Slider competitiveSlider;
    public Text competitiveSliderValue;
    public Slider warmthSlider;
    public Text warmthSliderValue;
    public Slider colourSlider;
    public Text colourSliderValue;
    public Slider numberSlider;
    public Text numberSliderValue;

    public Slider childrenSurviveSlider;
    public Text childrenSurviveSliderValue;
    public Slider adultSurviveSlider;
    public Text adultSurviveSliderValue;
    public Slider reproducedSlider;
    public Text reproducedSliderValue;

    //GeneBank
    public GameObject geneBankPanel;
    public GameObject genesClass;
    public GeneBank geneBankScript;
    public List<GameObject> genePanels = new List<GameObject>();

    //Terrain details
    public GameObject terrainTip;
    public Text terrainTipText;

    public List<GameObject> fitCreatures = new List<GameObject>();
    public List<GameObject> previousCreatures = new List<GameObject>();
    float panelWidth;
    float panelHeight;

    public Cursor cursorScript;

    public Image fitCreatureImage;
    public GameObject popGraph;


    public GameObject selected;
    public EnviroGeneration enviro;
    public GameObject cursor;

    public bool creaturePanelShow;
    public bool changeTile;
    public bool impactTile;

    public string geneType;

    public TerrainType holdingType;
    public ImpactType impactType;

    public Image terrainImage;
    public Sprite forestImageBig;
    public Sprite plainImageBig;
    public Sprite grasslandImageBig;
    public Sprite mountainImageBig;
    public Sprite cloudImageBig;
    public Sprite lakeImageBig;

    public float fitImageSize = 2;

    public GameObject core;

	// Use this for initialization
	void Start () {
        screenWidth = (float) (Camera.main.orthographicSize * 2.0 * Screen.width / Screen.height);
        screenHeight = (float)(Camera.main.orthographicSize * 2.0 * Screen.width / Screen.height);

     //   Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Done to remove stutter when clicking
            float timeCurrent = Time.timeScale;
            Time.timeScale = 0;

            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider != null)
            {
                if (!changeTile && !impactTile)
                {
                    if (hit.collider.gameObject.name == "AGroup")
                    {
                        if (selected != null)
                        {
                            selected.GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        selected = hit.collider.gameObject;
                        selected.GetComponent<SpriteRenderer>().color = Color.yellow;
                        geneticTableCurrent();
                        Debug.Log(hit.collider.gameObject.name);
                    }
                    if (hit.collider.gameObject.name == "BGroup")
                    {
                        if (selected != null)
                        {
                            selected.GetComponent<SpriteRenderer>().color = Color.white;
                        }
                        selected = hit.collider.gameObject;
                        selected.GetComponent<SpriteRenderer>().color = Color.yellow;
                        geneticTableCurrent();
                        Debug.Log(hit.collider.gameObject.name);
                    }

                }
                else if (changeTile)
                {
                    if (hit.collider.gameObject.name == "Tile")
                    {
                        selected = hit.collider.gameObject;
                        selected.GetComponent<Terrain>().setStats(holdingType,null);
                    }
                }
                else if (impactTile)
                {
                    if (hit.collider.gameObject.name == "Tile")
                    {
                        Debug.Log("Works");
                        selected = hit.collider.gameObject;
                        selected.GetComponent<Terrain>().impact(impactType, 0, 10);
                    }
                }
            }
            Time.timeScale = timeCurrent;
        }


        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (changeTile)
            {
                if (hit.collider.gameObject.name == "Tile")
                {
                    selected = hit.collider.gameObject;
                    selected.GetComponent<Terrain>().setStats(holdingType,null);
                }
            }
        }


            if (Input.GetKeyDown(KeyCode.Escape))
        {
            updateWindow = WindowUpdating.Nil;

            if (creaturePanelShow == false)
            {
                fitMainPanel.SetActive(false);
            }
            creaturePanel.SetActive(false);
            chromosomePanel.SetActive(false);
            geneBankPanel.SetActive(false);
            creaturePanelShow = false;

            if (holdingTest())
            {
                cursorScript.resetCursor();
            }
            holdingNothing();
        }
        if (selected != null && (Input.GetKeyDown("[1]") || Input.GetKeyDown("[2]") || Input.GetKeyDown("[3]") || Input.GetKeyDown("[4]") || Input.GetKeyDown("[6]") || Input.GetKeyDown("[7]") || Input.GetKeyDown("[8]") || Input.GetKeyDown("[9]")))
        {
            core.GetComponent<DarwinMachine>().pause();


            if (Input.GetKeyDown("[1]")) { selected.GetComponent<Group>().manualMove(1); }
            if (Input.GetKeyDown("[2]")) { selected.GetComponent<Group>().manualMove(2); }
            if (Input.GetKeyDown("[3]")) { selected.GetComponent<Group>().manualMove(3); }
            if (Input.GetKeyDown("[4]")) { selected.GetComponent<Group>().manualMove(4); }
            if (Input.GetKeyDown("[6]")) { selected.GetComponent<Group>().manualMove(6); }
            if (Input.GetKeyDown("[7]")) { selected.GetComponent<Group>().manualMove(7); }
            if (Input.GetKeyDown("[8]")) { selected.GetComponent<Group>().manualMove(8); }
            if (Input.GetKeyDown("[9]")) { selected.GetComponent<Group>().manualMove(9); }

            core.GetComponent<DarwinMachine>().singleTick();
        }

    }

    public void holdingNothing()
    {
        impactTile = false;
        changeTile = false;
//        holdingType = type;
    }

    public bool holdingTest()
    {
        if(impactTile || changeTile)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void holdingTile(TerrainType type)
    {
        impactTile = false;
        changeTile = true;
        holdingType = type;

    }

    public void holdingImpact(ImpactType type)
    {
        impactTile = true;
        changeTile = false;
        impactType = type;

    }

    //Lists all creatures in group fro most to least fit based off specific paparmeter in current environment
    public void geneticTableCurrent()
    {
        updateWindow = WindowUpdating.CurrentGenGroup;
        geneType = "fertility";
        fitMainPanel.SetActive(true);
        fitMainPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(screenWidth, screenHeight);
        Group selectedGroup = selected.GetComponent<Group>();
        List<Creature> creatures = selectedGroup.creatures;
        List<Image> images = new List<Image>();
        //Generate a cell for every creature in panel
        int creatureCount = creatures.Count;

        if(fitCreatures.Count == 0)
        {
            for(int i = 0; i < 1000; i++)
            {
                GameObject creature = Instantiate(fitCreature, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                creature.transform.SetParent(fitMainPanel.transform);
                creature.GetComponent<RectTransform>().sizeDelta = new Vector2(screenWidth * fitImageSize, screenHeight * fitImageSize);
                creature.GetComponent<RectTransform>().anchoredPosition = new Vector2(-550 + (i * 20) + ((i / 55) * -(20 * 55)), (screenHeight * 13) - ((i / 55) * 20));
                fitCreatures.Add(creature);
            }
        }

        if (fitCreatures.Count > 0)
        {

            for (int i = 0; i < fitCreatures.Count; i++)
            {
                if (i < 1000)
                {
                    fitCreatures[i].SetActive(false);
                }
            }
        }

            GetGridDimensions();


        for (int i = 0; i < creatureCount; i++)
        {
            if (i < 1000)
            {

                if (geneType == "fertility")
                {
                    float colNum = creatures[i].fertility;
                    fitCreatures[i].GetComponent<FitCreatureIcon>().loadCreatureCurrent(creatures[i], colNum,this);
                    fitCreatures[i].SetActive(true);
                }


            }
        }

        //Make list of all creature objects and sort by the specific parameter
    }

    public void switchTab()
    {

    }

    public void groupSummary()
    {
        if (selected != null)
        {
            if (selected.name == "AGroup")
            {
                speedSlider.value = selected.GetComponent<Group>().speed;
                speedSliderValue.text = selected.GetComponent<Group>().speed.ToString();
                sizeSlider.value = selected.GetComponent<Group>().size;
                sizeSliderValue.text = selected.GetComponent<Group>().size.ToString();
                fertilitySlider.value = selected.GetComponent<Group>().fertility;
                fertilitySliderValue.text = selected.GetComponent<Group>().fertility.ToString();
                warmthSlider.value = selected.GetComponent<Group>().warmth;
                warmthSliderValue.text = selected.GetComponent<Group>().warmth.ToString();
                colourSlider.value = selected.GetComponent<Group>().colour;
                colourSliderValue.text = selected.GetComponent<Group>().colour.ToString();
                numberSlider.value = selected.GetComponent<Group>().creatures.Count;
                numberSliderValue.text = selected.GetComponent<Group>().creatures.Count.ToString();

                childrenSurviveSlider.value = (float) selected.GetComponent<Group>().childFail / selected.GetComponent<Group>().creatures.Count;
                childrenSurviveSliderValue.text = ((float) selected.GetComponent<Group>().childFail / selected.GetComponent<Group>().creatures.Count).ToString();
                adultSurviveSlider.value = (float)selected.GetComponent<Group>().adultFail / selected.GetComponent<Group>().creatures.Count;
                adultSurviveSliderValue.text = ((float) selected.GetComponent<Group>().adultFail / selected.GetComponent<Group>().creatures.Count).ToString();
                reproducedSlider.value = (float) selected.GetComponent<Group>().reproductionFail / selected.GetComponent<Group>().creatures.Count;
                reproducedSliderValue.text = ((float) selected.GetComponent<Group>().reproductionFail / selected.GetComponent<Group>().creatures.Count).ToString();
            }
        }
    }


    public void geneticTablePrevious()
    {
        if (selected != null)
        {
            updateWindow = WindowUpdating.PreviousGenGroup;
            Group selectedGroup = selected.GetComponent<Group>();
            List<Creature> pCreatures = selectedGroup.lastGeneration;

            if (fitCreatures.Count == 0)
            {
                for (int i = 0; i < 1000; i++)
                {
                    GameObject creature = Instantiate(fitCreature, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    creature.transform.SetParent(fitMainPanel.transform);
                    creature.GetComponent<RectTransform>().sizeDelta = new Vector2(screenWidth * fitImageSize, screenHeight * fitImageSize);
                    creature.GetComponent<RectTransform>().anchoredPosition = new Vector2(-550 + (i * 20) + ((i / 55) * -(20 * 55)), (screenHeight * 13) - ((i / 55) * 20));
                    fitCreatures.Add(creature);
                }
            }

            if (fitCreatures.Count > 0)
            {
                for (int i = 0; i < fitCreatures.Count; i++)
                {
                    fitCreatures[i].SetActive(false);
                }
                fitCreatures.Clear();
            }

            for (int i = 0; i < pCreatures.Count; i++)
            {
                GameObject creature = Instantiate(fitCreature, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                creature.transform.SetParent(fitMainPanel.transform);
                creature.GetComponent<RectTransform>().sizeDelta = new Vector2(screenWidth * 5, screenHeight * 5);
                creature.GetComponent<RectTransform>().anchoredPosition = new Vector2(-550 + (i * 45) + ((i / 25) * -(45 * 25)), (screenHeight * 13) - ((i / 25) * 50));

                int colNum = 1;
                StageFailed typeFail = pCreatures[i].failedType;
                Sprite failSprite = failTypeSprite(typeFail);
                creature.GetComponent<FitCreatureIcon>().loadCreaturePrevious(pCreatures[i], failSprite, this);
                fitCreatures.Add(creature);
            }
        }

    }

    public void creatureWindowAppear(Creature creature)
    {
        creaturePanel.SetActive(true);
        creaturePanelShow = true;
        selectedCreature = creature;
        Sprite picture = creature.core.sprite;
        cPmainImage.sprite = picture;
        cPFather.sprite = picture;
        cPMother.sprite = picture;
        StageFailed failedType = creature.failedType;
        cPFailPoint.sprite = failTypeSprite(failedType);

        Debug.Log(creature.speed);

        speed.text = creature.speed.ToString();
        size.text = creature.size.ToString();
        competitive.text = creature.competitive.ToString();
        color.text = creature.color.ToString();
        warmth.text = creature.warmth.ToString();
        fertility.text = creature.fertility.ToString();


        int chromoCount = creature.chromosomes.Count;
      //  chromosomePanel.SetActive(true);
        //Loop through each chromosome. Creature genes under it??
        for (int i = 0; i < chromoCount; i = i + 2)
        {
            GameObject chromo = chromosomeGroups[i / 2];
            chromo.SetActive(true);
    //        chromo.transform.SetParent(chromosomePanel.transform);
            //         chromo.GetComponent<RectTransform>().sizeDelta = new Vector2(screenWidth * fitImageSize, screenHeight * fitImageSize);
            //        chromo.GetComponent<RectTransform>().anchoredPosition = new Vector2(-550 + (i * 50) + ((i / 55) * -(50 * 55)), (screenHeight * 13) - ((i / 55) * 20));
            chromo.transform.GetChild(0).GetComponent<ChromosomeImage>().chromosome = creature.chromosomes[i];
            chromo.transform.GetChild(1).GetComponent<ChromosomeImage>().chromosome = creature.chromosomes[i + 1];
        }
    }

    public void chromosomeWindowAppear()
    {
        int chromoCount = selectedCreature.chromosomes.Count;
        chromosomePanel.SetActive(true);
        //Loop through each chromosome. Creature genes under it??
        for (int i = 0; i < chromoCount; i = i+2)
        {
            GameObject chromo = chromosomeGroups[i/2];
            chromo.SetActive(true);
            chromo.transform.SetParent(chromosomePanel.transform);
   //         chromo.GetComponent<RectTransform>().sizeDelta = new Vector2(screenWidth * fitImageSize, screenHeight * fitImageSize);
    //        chromo.GetComponent<RectTransform>().anchoredPosition = new Vector2(-550 + (i * 50) + ((i / 55) * -(50 * 55)), (screenHeight * 13) - ((i / 55) * 20));
            chromo.transform.GetChild(0).GetComponent<ChromosomeImage>().chromosome = selectedCreature.chromosomes[i];
            chromo.transform.GetChild(0).GetComponent<ChromosomeImage>().chromosome = selectedCreature.chromosomes[i+1];
        }

    }

    public void geneBankAppear(int mod)
    {
        geneBankScript = genesClass.GetComponent<GeneGeneration>().bank;
        geneBankPanel.SetActive(true);
        for (int i = 0; i<genePanels.Count;i++)
        {
            Gene selectedGene = geneBankScript.genes[i];
            GameObject selectedPanel = genePanels[i];
            selectedPanel.GetComponent<GenePanel>().gene = selectedGene;
            Text firstChild = selectedPanel.transform.GetChild(0).GetComponent<Text>();
            firstChild.text = selectedGene.name + "\n" + selectedGene.type + "\n" + selectedGene.stats + "\n" + selectedGene.increase;
        }

    }

    public void toolTip(GameObject objectSelected)
    {
        cSize.text = objectSelected.GetComponent<ChromosomeImage>().chromosome.size.ToString();
        cWarmth.text = objectSelected.GetComponent<ChromosomeImage>().chromosome.warmth.ToString();
        cCompetitive.text = objectSelected.GetComponent<ChromosomeImage>().chromosome.competitive.ToString();
        cSpeed.text = objectSelected.GetComponent<ChromosomeImage>().chromosome.speed.ToString();
        cFertility.text = objectSelected.GetComponent<ChromosomeImage>().chromosome.fertility.ToString();
        cNumber.text = objectSelected.GetComponent<ChromosomeImage>().chromosome.number.ToString();
    }

    public Sprite failTypeSprite(StageFailed failedType)
    {
        if (failedType == StageFailed.Childhood)
        {
            return ftChildhood;
        }
        if (failedType == StageFailed.Adulthood)
        {
            return ftAdult;
        }
        if (failedType == StageFailed.Reproduction)
        {
            return ftReproduction;
        }
        if (failedType == StageFailed.Success)
        {
            return ftSuccess;
        }
        if (failedType == StageFailed.Alive)
        {
            return ftAlive;
        }
        return null;

    }

    public void terrainTipAppear(Terrain terrain, Vector3 position)
    {
        if (terrain.gameObject.GetComponent<SpriteRenderer>().sprite.name == "Cloud")
        {
            terrainImage.sprite = cloudImageBig;
            terrainTipText.text = "Unknown";
        }
        else
        {
            terrainTipText.text = terrain.getDetails();
            TerrainType type = terrain.getType();
            if (type == TerrainType.Plain1 || type == TerrainType.Plain2 || type == TerrainType.Plain3)
            {
                terrainImage.sprite = plainImageBig;
            }
            else if (type == TerrainType.Forest)
            {
                terrainImage.sprite = forestImageBig;
            }
            else if (type == TerrainType.Grassland1 || type == TerrainType.Grassland2)
            {
                terrainImage.sprite = grasslandImageBig;
            }
            else if (type == TerrainType.Sea)
            {
                terrainImage.sprite = lakeImageBig;
            }
            else if (type == TerrainType.Mountain)
            {
                terrainImage.sprite = mountainImageBig;
            }
        }

    }

    public void groupTipAppear(Group group, Vector3 position)
    {
        terrainTipText.text = group.getDetails();
    }

    public IEnumerator GetGridDimensions()
    {
        yield return new WaitForEndOfFrame();
        panelWidth = fitMainPanel.GetComponent<RectTransform>().sizeDelta.x;
        panelHeight = fitMainPanel.GetComponent<RectTransform>().sizeDelta.y;
   //     GridWidth = transform.GetComponent<RectTransform>().sizeDelta.x;
    }

    public void tick()
    {
        if(updateWindow == WindowUpdating.CurrentGenGroup)
        {
            geneticTableCurrent();
        }

        if (updateWindow == WindowUpdating.PreviousGenGroup)
        {
            geneticTablePrevious();
        }

    }

    public void updatePopGraph(int tickTime)
    {
        if (selected != null)
        {
            popGraph.GetComponent<StreamingGraph>().addData(tickTime, selected.GetComponent<Group>().creatures.Count);
        }
    }
}
