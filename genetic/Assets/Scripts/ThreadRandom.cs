﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This implementation of System.Random is thread safe.

public class ThreadRandom : MonoBehaviour
{
    // Start is called before the first frame update
    private static System.Random _global = new System.Random();
    [System.ThreadStatic]
    private static System.Random _local;

    public static int Next(int min, int max)
    {
        System.Random inst = _local;
        if (inst == null)
        {
            int seed;
            lock (_global) seed = _global.Next();
            _local = inst = new System.Random(seed);
        }
        return inst.Next(min,max);
    }

    public static double NextDouble()
    {
        System.Random inst = _local;
        if (inst == null)
        {
            int seed;
            lock (_global) seed = _global.Next();
            _local = inst = new System.Random(seed);
        }
        return inst.NextDouble();
    }
}
