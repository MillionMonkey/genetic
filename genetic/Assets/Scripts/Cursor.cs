﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour {

    bool active = false;
    bool resetPos = true;

    public Sprite cursorSprite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (active == true)
        {
            resetPos = false;
            Vector3 pos = Input.mousePosition;
            pos.z = transform.position.z - Camera.main.transform.position.z;
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, pos.z));
        }
        else
        {
            if (resetPos == false)
            {
                resetPos = true;
        //        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(-1000, -1000, -10));
            }
            
        }
    }

    public void changeCursor(Sprite sprite)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        active = true;
    }

    public void resetCursor()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = null;
        active = false;
    }
}
