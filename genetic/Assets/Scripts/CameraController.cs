﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float dragSpeed = 2;
    private Vector3 dragOrigin;

    public float cameraDistanceMax = 20f;
    public float cameraDistanceMin = 5f;
    float cameraDistance = 6f;
    float scrollSpeed = 8f;

    public GameObject groupLocked;
    public bool locked;

    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Time.timeScale = 0F;
            //      if ((Input.GetAxis("Mouse X") != 0) && (Input.GetAxis("Mouse Y") != 0))
            //      {
            dragOrigin = Input.mousePosition;

      //      }
            return;
        }

        if (Input.GetMouseButtonUp(1))
        {
            Time.timeScale = 1F;

        }

        if (locked == true)
        {
            if (groupLocked != null)
            {
                transform.position = new Vector3(groupLocked.transform.position.x, groupLocked.transform.position.y, transform.position.z);
            }
        }

        cameraDistance += Input.GetAxis("Mouse ScrollWheel") * -scrollSpeed;
        cameraDistance = Mathf.Clamp(cameraDistance, cameraDistanceMin, cameraDistanceMax);
        gameObject.GetComponent<Camera>().orthographicSize = cameraDistance;

        if (!Input.GetMouseButton(1)) return;

        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * -dragSpeed, pos.y * -dragSpeed, 0);
        transform.Translate(move, Space.World);

    //    Time.timeScale = 1F;
    }

    public void LockView(GameObject group)
    {
        if (locked == false)
        {
            groupLocked = group;
            locked = true;
        }
        else
        {
            locked = false;
        }
    }

}
