﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicGroup : MonoBehaviour {


    public List<string> debugGenes = new List<string>();
    public List<string> fitEvents = new List<string>();

    public float speed;
    public float size;
    public float warmth;
    public float competitive;
    public float children;


    public int numCreatures;
    public char[] origGene;
    public Species core;
    public bool start;
    public int count = 3;
    public int numberOfGenes;
    public GameObject environment;
    public EnviroGeneration environmentScript;
    public Fitness fitness;
    public GameObject groupPrefab;
    public int creatureCount;
    public int food;
    public int numberInSplit;
    public bool split = false;
    public bool dead;

    public List<Chromosome> startingChromosomes = new List<Chromosome>();
    public List<Chromosome> tempChromosomes = new List<Chromosome>();

    //Set in inspector
    public int chromosomePoolCount;
    public int chromosomeCount;

    public int dCount = 0;


    public Sprite sprite;

    string ret;

    public int bugCount;

    //Core stats
    public int gSize = 50;

    public bool first = false;

    public float debugSize;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //public void tileCalculation()
    //{
    //    food = environmentScript.returnFood((int)transform.position.x, (int)transform.position.y, 9, this);
    //    //      Debug.Log(food);
    //}

    //public void move()
    //{
    //    Vector3 location = environmentScript.getMove((int)transform.position.x, (int)transform.position.y, this.gameObject);
    //    gameObject.transform.position = location;
    //}
}
