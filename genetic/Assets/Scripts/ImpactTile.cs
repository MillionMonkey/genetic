﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ImpactTile : MonoBehaviour, IPointerClickHandler
{

    public ImpactType type;
    public GameObject cursor;
    public Cursor cursorScript;
    public GUICore guiScript;
    public GameObject GUI;

    // Use this for initialization
    void Start()
    {
        cursorScript = cursor.GetComponent<Cursor>();
        guiScript = GUI.GetComponent<GUICore>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        cursorScript.changeCursor(gameObject.GetComponent<Image>().sprite);
        guiScript.holdingImpact(type);
    }
}
