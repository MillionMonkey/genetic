﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseOver : MonoBehaviour {

    //When the mouse hovers over the GameObject, it turns to this color (red)
    Color s_MouseOverColor = Color.yellow;

    //This stores the GameObject’s original color
    Color s_OriginalColor;
    Color s_OriginalColor_Child;

    //Get the GameObject’s mesh renderer to access the GameObject’s material and color
    SpriteRenderer s_Renderer;
    SpriteRenderer s_Renderer_Child;

    public GUICore guiScript;

    void Start()
    {
        //Fetch the mesh renderer component from the GameObject
        s_Renderer = GetComponent<SpriteRenderer>();

        //Fetch the original color of the GameObject
        s_OriginalColor = s_Renderer.color;

        if (gameObject.transform.childCount > 0)
        {
            s_Renderer_Child = gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>();
            s_OriginalColor_Child = s_Renderer_Child.color;
        }
    }

    void OnMouseEnter()
    {
        // Change the color of the GameObject to red when the mouse is over GameObject
        if (gameObject.GetComponent<Terrain>() != null)
        {
            guiScript.terrainTipAppear(gameObject.GetComponent<Terrain>(), Input.mousePosition);
        } 
        else if (gameObject.GetComponent<Group>() != null)
        {
            guiScript.groupTipAppear(gameObject.GetComponent<Group>(), Input.mousePosition);

        }
        s_OriginalColor = s_Renderer.color;

        s_Renderer.color = s_MouseOverColor;
        if (gameObject.transform.childCount > 0)
        {
            s_OriginalColor_Child = s_Renderer_Child.color;
            s_Renderer_Child.color = s_MouseOverColor;
        }
    }

    void OnMouseExit()
    {

        // Reset the color of the GameObject back to normal
        s_Renderer.color = s_OriginalColor;
        if (gameObject.transform.childCount > 0)
        {
            s_Renderer_Child.color = s_OriginalColor_Child;
        }
    }


}
