﻿using System;
using UnityEngine;
using System.Collections;
using CielaSpike;
using System.Threading;

public class ExampleScript : MonoBehaviour {

    public GameObject test;
    public testScript testScript1;
    public int i;

    void Start()
    {
        testScript1 = test.GetComponent<testScript>();
        StartCoroutine(StartExamples());
    }

    void Update()
    {
        // rotate cube to see if main thread has been blocked;
        transform.Rotate(Vector3.up, Time.deltaTime * 180);
    }

    IEnumerator StartExamples()
    {
        Task task;
        this.StartCoroutineAsync(Blocking(), out task);
        yield return StartCoroutine(task.Wait());
        this.StartCoroutineAsync(Cancellation(), out task);
        yield return new WaitForSeconds(2.0f);
        task.Cancel();
        yield return this.StartCoroutineAsync(ErrorHandling(), out task);
    }

    IEnumerator Blocking()
    {
        Thread.Sleep(5000);
        yield return Ninja.JumpToUnity;
        yield return new WaitForSeconds(0.1f);
        Thread.Sleep(5000);
        yield return Ninja.JumpBack;
        yield return new WaitForSeconds(3.0f);
    }

    IEnumerator Cancellation()
    {
        for (int i = 0; i < int.MaxValue; i++)
        {
            // do some heavy ops;
            // ...
        }

        yield break;
    }

    IEnumerator ErrorHandling()
    {
        //LogAsync("Running heavy task...");
        for (int i = 0; i < int.MaxValue; i++)
        {
            this.i = testScript1.i;
            if (i > int.MaxValue / 2)
                throw new Exception("Some error from background thread...");
        }

        yield break;
    }

    //private void LogAsync(string msg)
    //{
    //    Debug.Log("[Async]" + msg);
    //}

    //private void LogState(Task task)
    //{
    //    Debug.Log("[State]" + task.State);
    //}

    //private void LogSync(string msg)
    //{
    //    Debug.Log("[Sync]" + msg);
    //}

    //private void LogExample(string msg)
    //{
    //    Debug.Log("[Example]" + msg);
    //}
}
