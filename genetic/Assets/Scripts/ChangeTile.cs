﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChangeTile : MonoBehaviour, IPointerClickHandler
{

    public TerrainType type;
    public GameObject cursor;
    public Cursor cursorScript;
    public GUICore guiScript;
    public GameObject GUI;

	// Use this for initialization
	void Start () {
        cursorScript = cursor.GetComponent<Cursor>();
        guiScript = GUI.GetComponent<GUICore>();

    }
	

    public void OnPointerClick(PointerEventData eventData)
    {
        cursorScript.changeCursor(gameObject.GetComponent<Image>().sprite);
        guiScript.holdingTile(type);
    }
}
