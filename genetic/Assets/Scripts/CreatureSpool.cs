﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureSpool : MonoBehaviour
{
    public List<Creature> creaturesAvailable1 = new List<Creature>();
    public List<Creature> creaturesAvailable2 = new List<Creature>();
    public int numInitialCreatures;
    public int extraCreaturesNum;
    public int totalCreatures;
    public int indexUpTo;
    public int lastGenIndex;
    public int listToUse;

    public GameObject core;
    public DarwinMachine coreScript;

    // Start is called before the first frame update
    void Start()
    {
        coreScript = core.GetComponent<DarwinMachine>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void initialCreatureGeneration()
    {
        indexUpTo = 0;
        for (int i = 0; i < numInitialCreatures; i++)
        {
            Creature creature = new Creature(coreScript);
            creaturesAvailable1.Add(creature);
            creature = new Creature(coreScript);
            creaturesAvailable2.Add(creature);
        }
        listToUse = 1;
        totalCreatures = numInitialCreatures;
    }

    public void bulkCreatureGeneration()
    {
        for (int i = 0; i < extraCreaturesNum; i++)
        {
            Creature creature = new Creature(coreScript);
            creaturesAvailable1.Add(creature);
            creature = new Creature(coreScript);
            creaturesAvailable2.Add(creature);
        }
        totalCreatures = totalCreatures + extraCreaturesNum;
    }

    public List<Creature> returnCreaturesUsed()
    {
        if (listToUse == 1)
        {
            return creaturesAvailable1.GetRange(0, indexUpTo);
        }
        else if (listToUse == 2)
        {
            return creaturesAvailable2.GetRange(0, indexUpTo);
        }
        return null;
    }

    public Creature returnNextCreature()
    {
        Creature creatureRet = null;
        if (listToUse == 1)
        {
            if (indexUpTo >= creaturesAvailable1.Count)
            {
                bulkCreatureGeneration();
            }
            creatureRet = creaturesAvailable1[indexUpTo];
    //        Debug.Log(1);
        }
        else
        {
            if (indexUpTo >= creaturesAvailable2.Count)
            {
                bulkCreatureGeneration();
            }
            creatureRet = creaturesAvailable2[indexUpTo];
   //         Debug.Log(2);
        }
        indexUpTo++;
        return creatureRet;
    }


    public Creature returnNextCreatureThread(int index)
    {
   //     Debug.Log(index);
        Creature creatureRet = null;
        if (listToUse == 1)
        {
            if (index >= creaturesAvailable1.Count)
            {
                bulkCreatureGeneration();
            }
            creatureRet = creaturesAvailable1[index];
            //        Debug.Log(1);
        }
        else
        {
            if (index >= creaturesAvailable2.Count)
            {
                bulkCreatureGeneration();
            }
            creatureRet = creaturesAvailable2[index];
            //         Debug.Log(2);
        }
        //      indexUpTo++;
   //     Debug.Log("creatureSpool" + index);
        indexUpTo++;

        return creatureRet;
    }

    public void endTick()
    {
        lastGenIndex = indexUpTo;
        if (listToUse == 1)
        {
            listToUse = 2;
        }
        else if (listToUse == 2)
        {
            listToUse = 1;
        }
        indexUpTo = 0;
    }
}
