﻿using UnityEngine;
using System.Collections;
using ChartAndGraph;

public class StreamingGraph : MonoBehaviour
{

    public GraphChart Graph;
    public int TotalPoints = 5;
    float lastTime = 0f;
    float lastX = 0f;

    public int tickYear;

    public GameObject core;

    public GameObject GUIScript;

    void Start()
    {
        if (Graph == null) // the ChartGraph info is obtained via the inspector
            return;
        float x = 3f * TotalPoints;
        Graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        Graph.DataSource.ClearCategory("Player 1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector

        for (int i = 0; i < TotalPoints; i++)  //add random points to the graph
        {
            Graph.DataSource.AddPointToCategory("Player 1", 1, Random.value * 20f + 10f); // each time we call AddPointToCategory 
            Graph.DataSource.AddPointToCategory("Player 1", 2, Random.value * 10f); // each time we call AddPointToCategory 
            x -= Random.value * 3f;
            lastX = x;
        }

        Graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
    }

    void Update()
    {
//        float time = Time.time;
//        if (lastTime + 2f < time)
//        {
//            lastTime = time;
//            lastX += Random.value * 3f;
////            System.DateTime t = ChartDateUtility.ValueToDate(lastX);

//        }

    }

    public void addData(int year, int data)
    {
        Graph.DataSource.StartBatch();
        Graph.DataSource.AddPointToCategoryRealtime("Player 1", core.GetComponent<DarwinMachine>().tickYear, GUIScript.GetComponent<GUICore>().selected.GetComponent<Group>().creatures.Count, 1f); // each time we call AddPointToCategory 
        Graph.DataSource.EndBatch();
    }
}
