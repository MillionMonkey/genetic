﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum GeneType
{
    Dominant,
    CoDominate,
    PolyGenic,
    Sex,
    IncompleteDominant,
    Recessive
}

public enum Stat
{
    Speed,
    Color,
    Size,
    Warmth,
    Competitive,
    Fertility,
    Special
}


public class GeneGeneration : MonoBehaviour {


    public List<GeneType> genetypes = new List<GeneType>();
    public List<Stat> stats = new List<Stat>();
    public List<int> increase = new List<int>();
    GeneType type;

    public GeneBank bank;

    public int geneCount = 1000;

    //    public GeneBank geneBank;

    // Use this for initialization
    void Start () {
        


  //      createGenes();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void createGenes()
    {
        foreach (GeneType geneType in (GeneType[])Enum.GetValues(typeof(GeneType)))
        {
            genetypes.Add(geneType);
        }
        foreach (Stat stat in (Stat[])Enum.GetValues(typeof(Stat)))
        {
            stats.Add(stat);
        }
        Enum.GetValues(typeof(GeneType)).Cast<GeneType>();

        bank = new GeneBank();
        for (int i = 0; i < geneCount; i++)
        {
            int typePick = (int)UnityEngine.Random.Range(0, genetypes.Count);
            int statPick = (int)UnityEngine.Random.Range(0, stats.Count);
            GeneType type = genetypes[typePick];
            Stat stat = stats[statPick];
            Gene gene = new Gene(type, stat, UnityEngine.Random.Range(-10, 10));
            bank.addGene(gene);
        }

    }
}
