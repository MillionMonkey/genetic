﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terrain : MonoBehaviour {



    public int food;
    public int x;
    public int y;
    public Hashtable groupFoodReq;
    public Hashtable groupFoodTemp;
    public Hashtable groupFoodRet;
    public List<Group> adjGroups;
    public EnviroGeneration enviro;
    public int retFood;
    public TerrainType type;

    public Sprite sprite;
    public SpriteRenderer renderer;
    public SpriteRenderer underRenderer;
    public Sprite transistionSprite;

    public Biome biome;

    public int temperature;

    public Sprite sea;
    public Sprite grassland2;
    public Sprite grassland1;
    public Sprite plain1;
    public Sprite trees;
    public Sprite plain2;
    public Sprite plain3;
    public Sprite mountain;
    public Sprite desert;
    public Sprite desolate;

    public bool start;

    public GameObject UnderTile;
    public AnimationScript underTileScript;

    public int distTravelled;
     public int impactSize;
    public ImpactType impactType;

    public bool visible;

    void Awake()
    {
        visible = false;
    }
    // Use this for initialization
    void Start () {

        x = (int) transform.position.x;
        y = (int) transform.position.y;

    }

    public int returnFood(Group group)
    {
        //Adjust food for groups nearby
        adjGroups = enviro.groupsNearTerrain(x, y);
        if (adjGroups.Count > 1)
        {
            return calcFoodRet(food, group);
        }
        return food;
    }

    //Called via animation
    public void changeSprite()
    {
        renderer.sprite = transistionSprite;
    }

    public void changeUnderSprite()
    {
        underRenderer.sprite = transistionSprite;
    }

    public void setStats(TerrainType type, Biome biome)
    {
        if (biome != null)
        {
            this.biome = biome;
        }
        if (start == true)
        {
            underTileScript = UnderTile.GetComponent<AnimationScript>();
        }

        if (this.type != type || start == true)
        {
            //      this.sprite = sprite;
            renderer = this.gameObject.GetComponent<SpriteRenderer>();
            underRenderer = UnderTile.GetComponent<SpriteRenderer>();

            //      renderer.sprite = sprite;
            this.type = type;
            Animation anim = GetComponent<Animation>();
            if (type == TerrainType.Plain1)
            {
                food = 800;
                transistionSprite = plain1;
                //       renderer.sprite = plain1;
            }
            if (type == TerrainType.Plain2)
            {
                food = 800;
                transistionSprite = plain2;
                //      renderer.sprite = plain2;
            }
            if (type == TerrainType.Plain3)
            {
                food = 800;
                transistionSprite = plain3;
                //       renderer.sprite = plain3;
            }
            if (type == TerrainType.Grassland1)
            {
                food = 1000;
                transistionSprite = grassland1;
                //       renderer.sprite = grassland1;
            }
            if (type == TerrainType.Desert)
            {
                food = 100;
                transistionSprite = desert;
                //       renderer.sprite = grassland1;
            }
            if (type == TerrainType.Desolate)
            {
                food = 50;
                transistionSprite = desolate ;
                //       renderer.sprite = grassland1;
            }
            if (type == TerrainType.Grassland2)
            {
                food = 1000;
                transistionSprite = grassland2;
                //     renderer.sprite = grassland2;
            }
            if (type == TerrainType.Sea)
            {
                food = 200;
                transistionSprite = sea;
                //    renderer.sprite = sea;
            }
            if (type == TerrainType.Mountain)
            {
                food = 200;
                transistionSprite = mountain;
                //     renderer.sprite = mountain;
            }
            if (type == TerrainType.Forest)
            {
                food = 1500;
                transistionSprite = trees;
                //      renderer.sprite = trees;
            }
            underTileScript.tileSprite = transistionSprite;

            if (visible == true)
            {
                underTileScript.playAnimation();
            }
            else
            {

            }
                
            
            start = false;
        }
    }

    public void show()
    {
        underTileScript.playAnimation();

    }

    public void impact(ImpactType type, int distTravelled, int impactSize)
    {
        float chance = (impactSize - distTravelled) * 10;

        this.distTravelled = distTravelled;
        this.impactSize = impactSize;
        impactType = type;

        if(distTravelled == 0) { chance = 100; }
        int ran = (int)UnityEngine.Random.Range(0, 100);
        if (ran < chance)
        {
            if (type == ImpactType.Meteor && this.type != TerrainType.Mountain)
            {
                //      this.sprite = sprite;
                renderer = this.gameObject.GetComponent<SpriteRenderer>();
                underRenderer = UnderTile.GetComponent<SpriteRenderer>();

                //      renderer.sprite = sprite;
                this.type = TerrainType.Desolate;
                Animation anim = GetComponent<Animation>();
                food = 200;
                transistionSprite = desolate;
                underTileScript.playImpactAnimation();
          //      enviro.impactTiles(type, x, y, distTravelled, impactSize);
            }

            if (type == ImpactType.Volcano && this.type != TerrainType.Mountain)
            {
                //      this.sprite = sprite;
                renderer = this.gameObject.GetComponent<SpriteRenderer>();
                underRenderer = UnderTile.GetComponent<SpriteRenderer>();

                //      renderer.sprite = sprite;
                this.type = TerrainType.Desert;
                Animation anim = GetComponent<Animation>();
                food = 200;
                transistionSprite = desert;
                underTileScript.playImpactAnimation();
                //      enviro.impactTiles(type, x, y, distTravelled, impactSize);
            }
        }
    }

    public void callEnviro()
    {
        underRenderer.sprite = transistionSprite;
        enviro.impactTiles(impactType, x, y, distTravelled, impactSize);
    }


    //Splits food amongst all in range groups depending on their needs and their competitiviness
    //When one group is saited their extra share is split amongst the remaining groups
    //Only called if there is more than 1 group
    public int calcFoodRet(int food, Group group)
    {
 //       groupFoodRet = new Hashtable();
        //Make list of all groups in range\
        adjGroups.Clear();
        adjGroups = enviro.groupsNearTerrain(x, y);

        float comp = 0;
        for(int i = 0; i< adjGroups.Count;i++)
        {
            //Add up all competitive stats of all groups
            comp = comp + adjGroups[i].competitive;

        }

        //Divide indiviual comp by total comp and get that portion of food
        int foodRet = 0;
        float gComp = group.competitive;
            //Add up all competitive stats of all groups
            foodRet = (int) (food * ((gComp) / comp));
    //    Debug.Log(food + " Food " + foodRet + " : " + gComp + " : "+ comp);
        return foodRet;
    }

    public void addGroup()
    {

    }

    public void setParameters()
    {


    }

    public TerrainType getType()
    {
        return type;
    }

    public string getDetails()
    {
        string details = "Type: " + type + "\n" + "Temperature: " + biome.temperature + "\n" + "Humidity: " + biome.humidity;
        return details;
    }
	
}
