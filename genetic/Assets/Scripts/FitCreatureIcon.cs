﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FitCreatureIcon : MonoBehaviour, IPointerClickHandler
{

    public float fitRep;
    public GUICore guiScript;

    public Creature creature;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void loadCreatureCurrent(Creature creature, float num, GUICore guiScript)
    {
        this.guiScript = guiScript;
        this.creature = creature;
        fitRep = num;
        if (num < 0)
        {
            //    gameObject.GetComponent<Image>().color = Color.black;
            gameObject.GetComponent<Image>().color = new Color(255, 0, 0);
        }
        if (num > 0)
        {
            gameObject.GetComponent<Image>().color = new Color(0, 255, 0);
        }
        if (num == 0)
        {
            gameObject.GetComponent<Image>().color = new Color(0, 0, 255);
        }
    }

    public void loadCreaturePrevious(Creature creature, Sprite failSprite, GUICore guiScript)
    {
        this.guiScript = guiScript;
        this.creature = creature;
        gameObject.GetComponent<Image>().sprite = failSprite;
    }


    public void changeColourCurrent(float num)
    {

    }

    public void changeColourPrevious(StageFailed typeFail)
    {
        if (typeFail == StageFailed.Childhood)
        {
            gameObject.GetComponent<Image>().color = Color.black;
        }
        if (typeFail == StageFailed.Adulthood)
        {
            gameObject.GetComponent<Image>().color = Color.red;
        }
        if (typeFail == StageFailed.Reproduction)
        {
            gameObject.GetComponent<Image>().color = Color.yellow;
        }
        if (typeFail == StageFailed.Success)
        {
            gameObject.GetComponent<Image>().color = Color.green;
        }

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        guiScript.creatureWindowAppear(creature);
    }
}
